#!/usr/bin/php
<?php
error_reporting(E_ALL);

require_once dirname(__FILE__)."/include/configuration.php";
require_once dirname(__FILE__)."/include/sql.php";

define('LOG_FILE_PATH',getenv('OPENSHIFT_LOG_DIR').'/schedule_fb_friend_log/'.date('Y-m-d').'.log');
define('QSTAT_PENDING',0);
define('QSTAT_SENDING',1);
define('QSTAT_SUCCESS',2);
define('QSTAT_FAILED',3);

function logx($text)
{
	
	echo $text;
	//return;
	$f = fopen(LOG_FILE_PATH,'a+');
	fwrite($f,$text);
	fwrite($f,"\n"); 	
	fclose($f);
	
}
function logy($text){
	$sql = sprintf("insert into com_autofbtwitter_log (descr,created_date,sender) values('%s','%s','schedule_fb_friend')",mysql_real_escape_string($text),date('Y-m-d H:i:s'));	
	$result=mysql_query($sql);
    if(!$result)
    {
        logx($sql."\r\n". mysql_error()."\r\n");
    } 
}
function getSchedule()
{
	$jf = new JConfig();
	$date = new DateTime(null,new DateTimeZone($jf->server_tz));	
	$current_time = $date->format('Y-m-d H:i:s');
	//echo $current_time;exit;		
	$sql = sprintf("
	SELECT com_autofbtwitter_fbfriendschedule.id as idx,com_autofbtwitter_fbfriendschedule.username,
	com_autofbtwitter_fbfriendschedule.message,
	com_autofbtwitter_fbfriendschedule.link,com_autofbtwitter_fbfriendschedule.image_file,
	com_autofbtwitter_fbfriendschedule.scheduled_date_time_server_time,
	com_autofbtwitter_fbfriendschedule.share_type,
	com_autofbtwitter_facebook.oauth_token	
	FROM com_autofbtwitter_fbfriendschedule		
	left join com_autofbtwitter_facebook on com_autofbtwitter_fbfriendschedule.com_autofbtwitter_account_id = com_autofbtwitter_facebook.com_autofbtwitter_account_id
	WHERE scheduled_date_time_server_time <= '%s' and state=%d",$current_time,QSTAT_PENDING);
	//logx($sql );
	return querysql($sql);	
}

function postmessage()
{		
	require_once(dirname(__FILE__).'/include/facebook.php'); 	
	$result = getSchedule();
	if ($result==null) {
	  $message  = 'Schedule is empty \n';
	  logx($message); return;
	}
	$jf = new JConfig();
	$app_id = $jf->app_id;
	$app_secret = $jf->app_secret;
	$upload_dir = $jf->upload_dir;
	$n=count($result);
	logx('schedule count: ' .$n); 
	
	$ids = array();	
	for($i=0;$i<$n;$i++)
	{
		$row = $result[$i];
		$ids[] = $row['idx'];		
	}	
	
	for($i=0;$i<$n;$i++)
	{
		$pid = pcntl_fork();
		if ($pid == -1) {
			logy("ERROR! Process fork failed for ".$i);
		} else if ($pid) {
			//parent
			$pids[]=$pid;
			connect(true); 
			continue;
		} else {
			$no = $i;
			$row = $result[$i];
			
			connect(true);
			$sql = sprintf("update com_autofbtwitter_fbfriendschedule set state=%d WHERE id = %d",QSTAT_SENDING,$row['idx']);
			//logx($sql);
			execsql($sql);	
			
			$status = $row['message'];							
			$oAuthToken = $row['oauth_token'];								
			$image_file = $row['image_file'];
			$link = $row['link'];				
			$share_type = $row['share_type'];
			$username = $row['username'];
			try{
				$facebook = new Facebook(array(
					 'appId' => $app_id,
					 'secret' => $app_secret,
					 'cookie' => true
					));
				logy('fb appId:' .$app_id .' app_secret:'. $app_secret.' oAuthToken:'. $oAuthToken .' message:' .$status); 
				$post = array('access_token' => $oAuthToken, 'message' => $status);
				
				//post a link
				if($share_type==1 && !empty($link)){
					$post['link'] = $link;
					$ret = $facebook->api("/$username/feed",'POST',$post);
				}
				//post image
				else if($share_type==0 && !empty($image_file)){
					$file_path = $upload_dir.'/'.$image_file;
					$post['image'] = '@' . realpath($file_path);
					logy('image: @' .$file_path);
					$facebook->setFileUploadSupport(true);				
					$ret = $facebook->api("/$username/photos",'POST',$post);	
				}else
				{
					//post message only
					$ret = $facebook->api("/$username/feed",'POST',$post);
				}
				ob_start();
				var_dump($ret);
				$result = ob_get_clean();
				logy($result);
				$state = QSTAT_SUCCESS;
				$error_msg='';
			} catch (Exception $e){
				logy($e->getMessage());
				$state = QSTAT_FAILED;
				$error_msg=$e->getMessage();
			}							
												
			$sql = sprintf("update com_autofbtwitter_fbfriendschedule 
			set state=%d,error_message='%s' 
			WHERE id = %d",$state,mysql_real_escape_string($error_msg),$row['idx']);
			//logx($sql);
			execsql($sql);
						
			exit;
		}		
	}
	//loop through until all children are finished
	//logx("Waiting for children to finish...");

	foreach ($pids as $pid) {
	    pcntl_waitpid($pid, $status);
	    if (pcntl_wifexited($status)) {
			logy('Process '.$pid.': normal exit');
	    } else {
			logy('Process '.$pid.': WARNING: abnormal process termination ('.pcntl_wexitstatus($status).')');
	    }
	}
	
}

function run()
{		
	connect();	
	$lockfile = getenv('OPENSHIFT_LOG_DIR')."/fbfriendschedule.lock";
	if(file_exists($lockfile))
	{
		logx("already running");return;	
	}
	$f = fopen($lockfile,"w+");
	fwrite($f,"");
	fclose($f);

	logx('running '.date ( 'Y-m-d H:i:s' ));
	
	postmessage();
	logx('finished '.date ( 'Y-m-d H:i:s' ));
	unlink($lockfile);

}
run();
?>
