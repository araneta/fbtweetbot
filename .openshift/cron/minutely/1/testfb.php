#!/usr/bin/php
<?php
error_reporting(E_ALL);

require_once dirname(__FILE__)."/include/configuration.php";
require_once dirname(__FILE__)."/include/sql.php";

define('LOG_FILE_PATH',getenv('OPENSHIFT_LOG_DIR').'/test-fb-'.date('Y-m-d').'.log');
define('QSTAT_PENDING',0);
define('QSTAT_SENDING',1);
define('QSTAT_SUCCESS',2);
define('QSTAT_FAILED',3);

function logx($text)
{	
	echo $text;
	//return;
	$f = fopen(LOG_FILE_PATH,'a+');
	fwrite($f,$text);
	fwrite($f,"\n"); 	
	fclose($f);	
}

function postmessage()
{		
	require_once(dirname(__FILE__).'/include/facebook.php'); 	
	
	$jf = new JConfig();
	$app_id = $jf->app_id;
	$app_secret = $jf->app_secret;
	$upload_dir = $jf->upload_dir;
	$status = 'test new server';							
	$oAuthToken = 'AAAGV120q8oQBALY723qRjG0Nnu5c3Nu96wvScCEya7Qgwfw944c7RcbonRQloyuIZCb7yZCC9SsGBk0O682MS2LT0K0pZB9ZAsMMStLY6QZDZD';								
	$image_file = '';
	$link = '';
	$caption = '';
	try{
		$facebook = new Facebook(array(
			 'appId' => $app_id,
			 'secret' => $app_secret,
			 'cookie' => true
			));
		logx('fb appId:' .$app_id .' app_secret:'. $app_secret.' oAuthToken:'. $oAuthToken .' message:' .$status); 
		$post = array('access_token' => $oAuthToken, 'message' => $status);
		if(!empty($link))
			$post['link'] = $link;
		if(!empty($caption))
			$post['caption'] = $caption;
		if(!empty($image_file)){
			$file_path = $upload_dir.'/'.$image_file;
			$post['image'] = '@' . realpath($file_path);			
			$facebook->setFileUploadSupport(true);				
			$ret = $facebook->api("/me/photos",'POST',$post);	
		}else
		{
			$ret = $facebook->api("/me/feed",'POST',$post);
		}
		ob_start();
		var_dump($ret);
		$result = ob_get_clean();
		logx($result);
		$state = QSTAT_SUCCESS;
		$error_msg='';
	} catch (Exception $e){
		logx($e->getMessage());
		$state = QSTAT_FAILED;
		$error_msg=$e->getMessage();
	}							
				
}

function run()
{		
	connect();	
	$lockfile = getenv('OPENSHIFT_LOG_DIR')."/test-fb.lock";
	if(file_exists($lockfile))
	{
		logx("already running");return;	
	}
	$f = fopen($lockfile,"w+");
	fwrite($f,"");
	fclose($f);

	logx('running '.date ( 'Y-m-d H:i:s' ));
	
	postmessage();
	logx('finished '.date ( 'Y-m-d H:i:s' ));
	unlink($lockfile);

}
run();
?>
