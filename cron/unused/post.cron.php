<?php
error_reporting(E_ALL);

require_once dirname(__FILE__)."/configuration.php";

function logx($text)
{
echo $text;
//return;
  $f = fopen(dirname(__FILE__).'/postmessagecronjob.log','a+');
  fwrite($f,$text);
  fwrite($f,"\n"); 	
  fclose($f);
}
function connect()
{
	$jf = new JConfig();
	$link = mysql_connect($jf->host, $jf->user, $jf->password);
	if (! $link)
      	{
	  logx("No access to MySQL server");return;
      	}
	if (!mysql_select_db ($jf->db, $link) )
	{
	  logx("No access to database: $jf->db: ".mysql_error()) ;return;
      	}

}
function execsql($validSql)
{

	$result=mysql_query($validSql);
    if(!$result)
    {
        logx($validSql."\r\n". mysql_error()."\r\n");
    }
}
function mysql_fetch_full_result_array($result)
{
    $table_result=array();
    $r=0;
    while($row = mysql_fetch_assoc($result)){
        $arr_row=array();
        $c=0;
        while ($c < mysql_num_fields($result)) {
            $col = mysql_fetch_field($result, $c);
            $arr_row[$col -> name] = $row[$col -> name];
            $c++;
        }
        $table_result[$r] = $arr_row;
        $r++;
    }
    return $table_result;
}
function querysql($validSql)
{
	
	$result=mysql_query($validSql);
    if(!$result)
    {
        logx($validSql."\r\n". mysql_error()."\r\n");
    }
    if(mysql_num_rows($result)>0)
	{
		return mysql_fetch_full_result_array($result);
	}
    return null;
}
function getQueue()
{
	//calculate current time
	$date   = date('Y-m-d');
	$hour   = date('g');
	$minute = date('i');
	$ampm   = date('a');
	$time = strtotime($date.' '.$hour.':'.$minute.' '.$ampm);
	$xtime = strtotime('+10 minutes', $time);
	$current_time  = date("Y-m-d G:i:s", $time);
	//echo $current_time;exit;	
	$jf = new JConfig();
	$sql = sprintf("SELECT * FROM %sposttwitter_log WHERE converted_time <= '%s' AND is_paid = 1",$jf->dbprefix,$current_time);
	logx($sql );
	return querysql($sql);	
}

function postmessage()
{	
	require_once(dirname(__FILE__).'/twitteroauth.php'); 
	require_once(dirname(__FILE__).'/facebook.php');
	$result = getQueue();
	if ($result==null) {
	  $message  = 'Queue is empty \n';
	  logx($message); return;
	}
	$jf = new JConfig();
	$n=count($result);
	logx('queue ' .$n); 
	for($i=0;$i<$n;$i++)
	{
		$row = $result[$i];
		$sql = sprintf("DELETE FROM %sposttwitter_log WHERE id = '%s'",$jf->dbprefix,$row['id']);
		execsql($sql);
		//update my cart
		//$sql = sprintf("update %sposttwitter_mycart SET  is_creative='3' where id='%s'",$jf->dbprefix,$row['cart_id']);
		//execsql($sql);	
	}
	for($i=0;$i<$n;$i++)
	{
		$pid = pcntl_fork();
		if ($pid == -1) {
			logx("ERROR! Process fork failed for ".$i);
		} else if ($pid) {
			//parent
			$pids[]=$pid;
			
			continue;
		} else {
			$no = $i;
			$row = $result[$i];
			if($row['account_type']== 'twitter')
			{
				$consumerkey       = trim($row['applicationid_or_consumerkey']);
				$consumersecretkey = trim($row['secretkey_or_consumersecretkey']);
				$oauthtoken        = trim($row['pageid_or_oauthtoken']);
				$oauthsecret       = trim($row['oauthsecret']);
				$status            = $row['message'];
			
				$consumerKey    = $consumerkey;
				$consumerSecret = $consumersecretkey;
				$oAuthToken     = $oauthtoken;
				$oAuthSecret    = $oauthsecret;
				logx('twitter consumerKey:' .$consumerKey .' consumerSecret:'. $consumerSecret.' oAuthToken:'. $oAuthToken.' oAuthSecret:'. $oAuthSecret.' username:' .$row['username'].' message:' .$row['message']); 
				$tweet = new TwitterOAuth($consumerKey, $consumerSecret, $oAuthToken, $oAuthSecret);
				$ret = $tweet->post('statuses/update', array('status' => $status));	
				ob_start();
				var_dump($ret);
				$result = ob_get_clean();
				logx($result);
			}else
			{
				$consumerkey       = trim($row['applicationid_or_consumerkey']);
				$consumersecretkey = trim($row['secretkey_or_consumersecretkey']);
				$oauthtoken        = trim($row['pageid_or_oauthtoken']);
				$oauthsecret       = trim($row['oauthsecret']);
				$status            = $row['message'];
				
				$post = array('access_token' => $oauthsecret, 'message' => $status);

				try{
					$facebook = new Facebook(array(
						 'appId' => $consumerkey,
						 'secret' => $consumersecretkey,
						 'cookie' => true
						));
					logx('fb consumerKey:' .$consumerKey .' consumerSecret:'. $consumersecretkey.' oAuthToken:'. $oauthtoken.' oauthsecret:'. $oauthsecret.' message:' .$status); 
					$ret = $facebook->api("/$oauthtoken/feed",'POST',$post);
					ob_start();
							var_dump($ret);
							$result = ob_get_clean();
							logx($result);
				} catch (Exception $e){
					logx($e->getMessage());
					echo $e->getMessage();
				}
			}
			
			exit;
		}		
	}
	//loop through until all children are finished
	logx("Waiting for children to finish...");

	foreach ($pids as $pid) {
	    pcntl_waitpid($pid, $status);
	    if (pcntl_wifexited($status)) {
		logx('Process '.$pid.': normal exit');
	    } else {
		logx('Process '.$pid.': WARNING: abnormal process termination ('.pcntl_wexitstatus($status).')');
	    }
	}
		
}

function run()
{		
	logx('running'.date ( 'Y-m-d H:i:s' ));
	$lockfile = dirname(__FILE__)."/post.lock";
	if(file_exists($lockfile))
	{
		logx("already running");return;	
	}
	$f = fopen($lockfile,"w+");
	fwrite($f,"");
	fclose($f);

	logx('running '.date ( 'Y-m-d H:i:s' ));
	connect();
	postmessage();
	logx('finished '.date ( 'Y-m-d H:i:s' ));
	unlink($lockfile);

}
run();
?>
