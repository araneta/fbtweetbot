<?php
error_reporting(E_ALL);

require_once dirname(__FILE__)."/configuration.php";
require_once dirname(__FILE__)."/sql.php";

define('LOG_FILE_PATH',dirname(__FILE__).'/queue_tweet_log/'.date('Y-m-d').'.log');
define('QSTAT_PENDING',0);
define('QSTAT_SENDING',1);
define('QSTAT_SUCCESS',2);
define('QSTAT_FAILED',3);

function logx($text)
{
	
	echo $text;
	//return;
	$f = fopen(LOG_FILE_PATH,'a+');
	fwrite($f,$text);
	fwrite($f,"\n"); 	
	fclose($f);
	
}
function logy($text){
	$sql = sprintf("insert into com_autofbtwitter_log (descr,created_date,sender) values('%s','%s','queue_tweet')",mysql_real_escape_string($text),date('Y-m-d H:i:s'));	
	$result=mysql_query($sql);
    if(!$result)
    {
        logx($sql."\r\n". mysql_error()."\r\n");
    } 
}
function getqueue()
{
	$jf = new JConfig();
	$date = new DateTime(null,new DateTimeZone($jf->server_tz));	
	$current_time = $date->format('Y-m-d H:i:s');
	//echo $current_time;exit;		
	$sql = sprintf("
	SELECT com_autofbtwitter_queue_message.id as idx,com_autofbtwitter_queue_message.message,
	com_autofbtwitter_queue_message.scheduled_date_time_server_time,
	com_autofbtwitter_account.type,com_autofbtwitter_account.username,	
	com_autofbtwitter_twitter.*,
	com_autofbtwitter_queue_message.image_file
	FROM com_autofbtwitter_queue_message
	inner join com_autofbtwitter_queue on com_autofbtwitter_queue_message.com_autofbtwitter_queue_id = com_autofbtwitter_queue.id
	inner join com_autofbtwitter_account on com_autofbtwitter_queue.com_autofbtwitter_account_id = com_autofbtwitter_account.id	
	left join com_autofbtwitter_twitter on com_autofbtwitter_queue.com_autofbtwitter_account_id = com_autofbtwitter_twitter.com_autofbtwitter_account_id
	WHERE scheduled_date_time_server_time <= '%s' and state=%d",$current_time,QSTAT_PENDING);
	//logx($sql );
	return querysql($sql);	
}

function postmessage()
{		
	require_once(dirname(__FILE__).'/tmhOAuth.php');
	require_once(dirname(__FILE__).'/tmhUtilities.php');	
	$result = getqueue();
	if ($result==null) {
	  $message  = 'queue is empty \n';
	  logx($message); return;
	}
	$jf = new JConfig();
	$consumerKey    = $jf->consumer_key;
	$consumerSecret = $jf->consumer_secret;
	$upload_dir = $jf->upload_dir;
	$n=count($result);
	logx('queue count: ' .$n); 
	
	$ids = array();	
	for($i=0;$i<$n;$i++)
	{
		$row = $result[$i];
		$ids[] = $row['idx'];		
	}	
	
	for($i=0;$i<$n;$i++)
	{
		$pid = pcntl_fork();
		if ($pid == -1) {
			logy("ERROR! Process fork failed for ".$i);
		} else if ($pid) {
			//parent
			$pids[]=$pid;
			connect(true); 
			continue;
		} else {
			$no = $i;
			$row = $result[$i];
			if($row['type']== '1')
			{
				connect(true);
				$sql = sprintf("update com_autofbtwitter_queue_message set state=%d WHERE id = %d",QSTAT_SENDING,$row['idx']);
				//logx($sql);
				execsql($sql);	
				
				$status            = $row['message'];							
				$oAuthToken     = $row['oauth_token'];
				$oAuthSecret    =$row['oauth_token_secret'];
				$image_file = $row['image_file'];
				logy('twitter consumerKey:' .$consumerKey .' consumerSecret:'. $consumerSecret.' oAuthToken:'. $oAuthToken.' oAuthSecret:'. $oAuthSecret.' username:' .$row['username'].' message:' .$row['message'].' file:'.$image_file); 
				//$tweet = new TwitterOAuth($consumerKey, $consumerSecret, $oAuthToken, $oAuthSecret);
				//$ret = $tweet->post('statuses/update', array('status' => $status));	
				$tmhOAuth = new tmhOAuth(array(
					 'consumer_key'    => $consumerKey ,
					 'consumer_secret' => $consumerSecret,
					 'user_token'      => $oAuthToken,
					 'user_secret'     => $oAuthSecret,
				));
				if(empty($image_file)){
					$code = $tmhOAuth->request('POST', $tmhOAuth->url('1/statuses/update'), array(
					  'status' => $status
					));
					if ($code == 200){
						$state = QSTAT_SUCCESS;
						$error_msg='';						
						$result = '';
					}else{
						$state = QSTAT_FAILED;
						$error_msg=$tmhOAuth->response['response'];
						$result = $error_msg;
					}
					logy($result);	
				}else{										
					$image = $upload_dir.'/'.$image_file;
					//$file = escapeshellarg('/home/aldo/Pictures/1.png');
					//$mime = shell_exec("file -bi " . $file);
					//$mimetype = substr($mime,0,strpos($mime,';'));
					$code = $tmhOAuth->request( 'POST','https://api.twitter.com/1.1/statuses/update_with_media.json',
					   array(
							'media[]'  => "@{$image};type=image/jpeg;filename={$image}",
							'status'   => $status,
					   ),
						true, // use auth
						true  // multipart
					);

					if ($code == 200){
						$state = QSTAT_SUCCESS;
						$error_msg='';
						$data = json_decode($tmhOAuth->response['response']);				
						$entities = $data->{'entities'};
						$media =$entities->{'media'};
						$xmedia = $media[0];
						$result = 'http://'.$xmedia->{'display_url'};
					}else{
						$state = QSTAT_FAILED;
						$error_msg=$tmhOAuth->response['response'];
						$result = $error_msg;
					}
					logy($result);		
				}								
				$sql = sprintf("update com_autofbtwitter_queue_message 
				set state=%d,error_message='%s' 
				WHERE id = %d",$state,$error_msg,$row['idx']);
				//logx($sql);
				execsql($sql);
			}			
			exit;
		}		
	}
	//loop through until all children are finished
	//logx("Waiting for children to finish...");

	foreach ($pids as $pid) {
	    pcntl_waitpid($pid, $status);
	    if (pcntl_wifexited($status)) {
			logy('Process '.$pid.': normal exit');
	    } else {
			logy('Process '.$pid.': WARNING: abnormal process termination ('.pcntl_wexitstatus($status).')');
	    }
	}
	
}

function run()
{	
	connect();	
	
	$lockfile = dirname(__FILE__)."/queue.lock";
	if(file_exists($lockfile))
	{
		logx("already running");return;	
	}
	$f = fopen($lockfile,"w+");
	fwrite($f,"");
	fclose($f);

	logx('running '.date ( 'Y-m-d H:i:s' ));
	
	postmessage();
	logx('finished '.date ( 'Y-m-d H:i:s' ));
	unlink($lockfile);

}
run();
?>
