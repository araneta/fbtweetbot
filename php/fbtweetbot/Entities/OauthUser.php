<?php
namespace fbtweetbot\Entities;
use core\Entities\BaseEntity;

class OauthUser extends User{
	/**
	 * oauth user id
	 * @var int $id
	 */	 
	public $id;
	/**
	 * user id, reference to User class
	 * @var int $userid
	 */	 
	public $userid;
	/**
	 * oauth provider type, for example: facebook, twitter
	 * @var string $oauth_provider
	 */
	public $oauth_provider;
	/**
	 * oauth id
	 * @var string $oauth_uid
	 */
	public $oauth_uid;
	/**
	 * oauth username
	 * @var string $oauth_username
	 */
	public $oauth_username;
	/**
	 * email from oauth provider
	 * @var string $email
	 */
	public $email;
	/**
	 * record created date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $created_date;
	/**
	 * record updated date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $last_update;
	
	public function __construct(){
		parent::__construct();
	}
}
