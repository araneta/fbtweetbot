<?php
namespace fbtweetbot\Entities;
use core\Entities\BaseEntity;

class LoginForm extends BaseEntity{
	public $username;
	public $password;
	
	public function __construct(){
		parent::__construct();
	}
	
	public function validate(){
		$this->required(array('username','password'));
		return !$this->has_error();
	}
}
