<?php
namespace fbtweetbot\Entities;
use core\Entities\BaseEntity;

class LocalUser extends User{
	//constant
	public $user_type = 1;
	/**
	 * local user id
	 * @var int $id
	 */	 
	public $id;
	/**
	 * user id of User
	 * @var int $userid
	 */
	public $userid;
	/**
	 * username
	 * @var string $username
	 */
	public $username;
	/**
	 * encrypted password
	 * @var string $password
	 */
	public $password;
	/**
	 * email
	 * @var string $email
	 */
	public $email;
	/**
	 * record created date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $created_date;
	/**
	 * record updated date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $last_update;
	
	public function __construct(){
		parent::__construct();
	}
}
