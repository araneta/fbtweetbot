<?php
namespace fbtweetbot\Entities;
use core\Entities\BaseEntity;

class User extends BaseEntity{
	const LOCAL_USER = 1;
	const OAUTH_USER = 2;
	/**
	 * user id
	 * @var int $id
	 */
	public $id;
	/**
	 * user type: 1:local user, 2:user login using external services
	 * @var int $user_type
	 */
	public $user_type;
	/**
	 * user role: admin, user
	 * @var string $role
	 */
	public $role;
	/**
	 * user last login in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $lastlogin 
	 */
	public $last_login;
	/**
	 * user time zone
	 * @var string $timezone
	 */
	public $timezone;
	/**
	 * language id, foreign key of language table
	 * @var int id_language
	 */
	public $id_language;
	/**
	 * record created date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $created_date;
	/**
	 * record updated date in yyyy-mm-dd hh:mm:ss format
	 * @var datetime $created_date
	 */
	public $last_update;
	
	public function __construct(){
		parent::__construct();
	}
}
