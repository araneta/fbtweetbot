<?php
namespace fbtweetbot\Mappers;

use core\Entities\PagingResult;
use core\Entities\KeyValue;
use core\Mappers\BaseMapper;

use fbtweetbot\Entities\User;
use fbtweetbot\Entities\LocalUser;

class UserMapper extends BaseMapper{
	private $get_local_user_by_username_sql = 'select * from local_user where username = ?';
	private $update_last_login_sql = 'update user set last_login=?, last_update=? where id = ?';
	private $get_user_by_id_sql = 'select * from user where id=?';
	
	public function __construct(){
		parent::__construct();
	}
	public function get_local_user_by_username($username){
		$rows = $this->get_rows($this->get_local_user_by_username_sql,array($username));
		if($rows!=NULL){
			$user = new LocalUser();
			$user->bind($rows[0]);
			return $user;
		}
		return NULL;
	}
	public function update_last_login(User $user){
		$data = array($user->last_login,$user->last_update,$user->id);
		return $this->execute($this->update_last_login_sql,$data);
	}
	public function get_user_by_id($userid){
		$rows = $this->get_rows($this->get_user_by_id_sql,array($userid));
		if($rows!=NULL){
			$user = new User();
			$user->bind($rows[0]);
			return $user;
		}
		return NULL;
	}
	
}
