<?php
namespace fbtweetbot\Tests;


define('BASEPATH',realpath(dirname(__FILE__).'/../'));
//define('LOCK_FILE','reminder.lock');
//define('LOCK_FILE_PATH', dirname(__FILE__).'/'.LOCK_FILE);
define('LOG_FILE','reminder.log');
define('LOG_FILE_PATH', dirname(__FILE__).'/'.LOG_FILE);

class Bootstrap{
	static public function autoloader($className)
	{	
		if (strpos($className, 'CI_') !== 0 && strpos($className,'PHPUnit')!==0)
		{
			
			$className = ltrim($className, '\\');
			$fileName  = '';
			$namespace = '';
			if ($lastNsPos = strrpos($className, '\\')) {
				$namespace = substr($className, 0, $lastNsPos);
				$className = substr($className, $lastNsPos + 1);
				$fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
			}
			$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';		
			require dirname(BASEPATH).DIRECTORY_SEPARATOR.$fileName;		
		}
	} 
	static public  function log_status($text){	
		$f = fopen(LOG_FILE_PATH,'a+');
		fwrite($f, TimeHelper::get_time_in_utc()." - ".$text."\r\n");
		fclose($f);
	}
	
}	
error_reporting(E_ALL);
spl_autoload_register(__NAMESPACE__ .'\Bootstrap::autoloader');
