<?php
namespace fbtweetbot\Tests;
use core\Helpers\DBHelper;
use core\Helpers\TimeHelper;
use fbtweetbot\FBTFactory;
use fbtweetbot\Configs\DBConfig;
use fbtweetbot\Entities\LoginForm;


//require_once 'PHPUnit/Autoload.php';
class UserTest extends \PHPUnit_Framework_TestCase {
	private $Gateway;	
	public function setUp() {		
		$helper = new DBHelper();
		$helper->set_connection_params(DBConfig::HOST,DBConfig::USERNAME,DBConfig::PASSWORD,DBConfig::DBNAME);
		FBTFactory::set_database($helper);
	}
	
	public function testValidLocalUserLogin(){
		$service = FBTFactory::get_user_service();	
		$form = new LoginForm();
		$form->username = 'bejo';
		$form->password = 'willamette';
		$this->assertTrue($service->local_user_login($form));
	}
	public function testInvalidLocalUserLogin(){
		$service = FBTFactory::get_user_service();	
		$form = new LoginForm();
		$form->username = 'bejo';
		$form->password = 'P@ssw0rd';
		$this->assertFalse($service->local_user_login($form));
	}
}
