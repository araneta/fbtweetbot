<?php
namespace fbtweetbot\Tests;
use core\Helpers\DBHelper;
use core\Helpers\TimeHelper;
use fbtweetbot\FBTFactory;
use fbtweetbot\Configs\CommonConfig;
use fbtweetbot\Configs\FBConfig;
use fbtweetbot\Helpers\Facebook\BaseFacebook;
use fbtweetbot\Helpers\Facebook\Facebook;


//require_once 'PHPUnit/Autoload.php';
class FBTest extends \PHPUnit_Framework_TestCase {
	private $Gateway;	
	private $facebook;
 	
	/**
	* Checks that the correct args are a subset of the returned obj
	* @param  array $correct The correct array values
	* @param  array $actual  The values in practice
	* @param  string $message to be shown on failure
	*/
	protected function assertIsSubset($correct, $actual, $msg='') {
    	foreach ($correct as $key => $value) {
      		$actual_value = $actual[$key];
      		$newMsg = (strlen($msg) ? ($msg.' ') : '').'Key: '.$key;
      		$this->assertEquals($value, $actual_value, $newMsg);
    	}
  	}
	private static function kValidSignedRequest($id = FBConfig::TEST_USER, $oauth_token = null) {
	    $facebook = new FBPublic(array(
	      'appId'  => FBConfig::APP_ID,
	      'secret' => FBConfig::SECRET,
	    ));
	    return $facebook->publicMakeSignedRequest(
	      array(
	        'user_id' => $id,
	        'oauth_token' => $oauth_token
	      )
	    );
	}
	public function setUp() {		
		//$helper = new DBHelper();
		//$helper->set_connection_params(DBConfig::HOST,DBConfig::USERNAME,DBConfig::PASSWORD,DBConfig::DBNAME);
		//FBTFactory::set_database($helper);
		$this->facebook = FBTFactory::get_facebook_service();
		
	}
	public function testLoginUrlWithReadPublishStreamEmail(){
		// fake the HPHP $_SERVER globals
	    $_SERVER['HTTP_HOST'] = 'www.test.com';
	    $_SERVER['REQUEST_URI'] = '/unit-tests.php';
     	$url = $this->facebook->get_login_url();
	    //echo 'fb login url'.$url;
	    $login_url = parse_url($url);
	    $this->assertEquals($login_url['scheme'], 'https');
	    $this->assertEquals($login_url['host'], 'www.facebook.com');
	    $this->assertEquals($login_url['path'], '/dialog/oauth');
	    $expected_login_params =
	      array('client_id' => FBConfig::APP_ID,
	            'redirect_uri' => CommonConfig::LOGIN_PAGE);
	
	    $query_map = array();
	    parse_str($login_url['query'], $query_map);
	    $this->assertIsSubset($expected_login_params, $query_map);
	    // we don't know what the state is, but we know it's an md5 and should
	    // be 32 characters long.
	    $this->assertEquals(strlen($query_map['state']), $num_characters = 32);
	}
	public function testGetFacebokUser(){	
		 $facebook = new TransientFacebook(array(
	      'appId'  => FBConfig::APP_ID,
	      'secret' => FBConfig::SECRET,
	    ));
	
	    $_REQUEST['signed_request'] = self::kValidSignedRequest();
	    $this->assertEquals('499834690', $facebook->getUser(),
	                        'Failed to get user ID from a valid signed request.');
	   	$user = $this->facebook->get_facebook_user();
	   	//var_dump($user);
	   	$this->assertNotNull($user); 
	}
}

class TransientFacebook extends BaseFacebook {
  protected function setPersistentData($key, $value) {}
  protected function getPersistentData($key, $default = false) {
    return $default;
  }
  protected function clearPersistentData($key) {}
  protected function clearAllPersistentData() {}
}
class FBPublic extends TransientFacebook {
  public static function publicBase64UrlDecode($input) {
    return self::base64UrlDecode($input);
  }
  public static function publicBase64UrlEncode($input) {
    return self::base64UrlEncode($input);
  }
  public function publicParseSignedRequest($input) {
    return $this->parseSignedRequest($input);
  }
  public function publicMakeSignedRequest($data) {
    return $this->makeSignedRequest($data);
  }
}