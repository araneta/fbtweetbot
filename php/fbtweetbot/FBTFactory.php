<?php
namespace fbtweetbot;

use core\BaseFactory;
use fbtweetbot\Services\UserService;
use fbtweetbot\Services\FBTRoleCheckService;
use fbtweetbot\Services\FacebookService;
use fbtweetbot\Entities\User;

class FBTFactory extends BaseFactory{
	/**
	 * handle user service instance 
	 * UserService $user_service
	 */
	private static $user_service = NULL;
	/**
	 * handle role check service instance
	 * FBTRoleCheckService $role_check_service
	 */
	 private static $role_check_service = NULL;
	 /**
	 * handle facebook service instance
	 * FacebookService $facebook_service
	 */
	 private static $facebook_service = NULL;
	/**
	 * active user
	 * User $active_user
	 */
	private static $active_user = NULL;
	
	private function __construct(){
		
	}
	//override
	public function create_database_object(){
		//since this gss is using codeigniter, it just use the db connection from codeigniter
		return self::$database;
	}	
	/**
	 * get user service instance
	 * return instance of UserService
	 */
	public static function get_user_service(){	
		if(self::$user_service==NULL)
			self::$user_service = new UserService();
		return 	self::$user_service ;	
	}
	/**
	 * get role check service instance
	 * return instance of FBTRoleCheckService
	 */
	public static function get_role_check_service(){	
		if(self::$role_check_service==NULL)
			self::$role_check_service = new FBTRoleCheckService();
		return 	self::$role_check_service ;	
	}
	/**	
	 * get facebook service instance
	 * return instance of FacebookService
	 */
	public static function get_facebook_service(){	
		if(self::$facebook_service==NULL)
			self::$facebook_service = new FacebookService();
		return 	self::$facebook_service ;	
	}
	/**
	 * set active user
	 * @var User $user
	 * return void
	 */
	public static function set_active_user(User $user){
		self::$active_user = $user;
	}
	/**
	 * get active user
	 * return User
	 */
	public static function get_active_user(){
		return self::$active_user;
	}
}
?>
