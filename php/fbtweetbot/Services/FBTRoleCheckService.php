<?php
namespace fbtweetbot\Services;
use core\Services\SimpleRoleCheckService;

class FBTRoleCheckService extends SimpleRoleCheckService{
	private $user_roles = array(
		'public'	=> array('id'=>1,'parent'=>NULL),
		'registered'=> array('id'=>2,'parent'=>'public'),
		'member'	=> array('id'=>3,'parent'=>'registered'),
		'admin'		=> array('id'=>4,'parent'=>'registered'),
		'presenter'		=> array('id'=>5,'parent'=>'member'),
	);
	//override
	public function get_roles(){
		return $this->user_roles;
	}
	public function __construct(){
		parent::__construct();
	}
}
