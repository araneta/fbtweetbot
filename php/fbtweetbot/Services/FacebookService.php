<?php
namespace fbtweetbot\Services;

use core\Services\BaseService;
use fbtweetbot\FBTFactory;
use fbtweetbot\Configs\FBConfig;
use fbtweetbot\Configs\CommonConfig;
use fbtweetbot\Helpers\Facebook\Facebook;

class FacebookService extends BaseService {
	/**
	 *  facebook class instance
	 */	
	private $facebook;
	function __construct(){
		parent::__construct();
		$this->init_facebook();
	}
	public function init_facebook(){
		$this->facebook = new Facebook(array(
                'appId'  => FBConfig::APP_ID,
                'secret' => FBConfig::SECRET,
         ));
	}
	/**
	 * get facebook login url
	 * @return String
	 */
	public function get_login_url(){
		$params = array(
			'redirect_uri' =>CommonConfig::LOGIN_PAGE,
//			'canvas'=>1,
//			'fbconnect'=>0,
			'scope'=>'read_stream,publish_stream,email'
		);
    	$login_url = $this->facebook->getLoginUrl($params);
    	return $login_url;
	}
	/**
	 * get facebook logged in
	 * return array
	 * array(7) {
	  ["id"]=>
	  string(9) "499834690"
	  ["name"]=>
	  string(14) "PHPSDKTestCase"
	  ["first_name"]=>
	  string(14) "PHPSDKTestCase"
	  ["last_name"]=>
	  string(14) "PHPSDKTestCase"
	  ["link"]=>
	  string(55) "http://www.facebook.com/people/PHPSDKTestCase/499834690"
	  ["gender"]=>
	  string(4) "male"
	  ["locale"]=>
	  string(5) "en_US"
	 */
	public function get_facebook_user(){
		$user = 0;
		for($i=0;$i<100;$i++){		
			//return string userid ex:499834690
			$user = $this->facebook->getUser();
			if($user!=0)
				break;			
		}
		//echo "fbid".$user;
		$profile = null;
		$fb_user = null;
		if($user)
		{					
			try {
				$accessToken = $this->facebook->getAccessToken();								
				//$fb_user = $this->facebook->api('/me');								
				$fb_user = $this->facebook->api('/'.$user);					
                //$ses_user=array('fb_user'=>$fb_user);
		        //$this->session->set_userdata($ses_user);
		        //var_dump($fb_user);
            } catch (FacebookApiException $e) {
				var_dump($e);
				exit(0);
                $fb_user = null;
            }
		}
		return $fb_user;
	}
}
