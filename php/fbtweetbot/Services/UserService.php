<?php
namespace fbtweetbot\Services;

use core\Services\BaseService;
use core\Helpers\TimeHelper;
use core\Helpers\BcryptHelper;
use fbtweetbot\FBTFactory;
use fbtweetbot\Entities\User;
use fbtweetbot\Entities\LoginForm;
use fbtweetbot\Mappers\UserMapper;

class UserService extends BaseService {	
	private $user_mapper;
	private $current_user;
	
	function __construct(){
		parent::__construct();
	}
	
	protected function get_user_mapper(){
		if($this->user_mapper==NULL){
			$this->user_mapper = new UserMapper();
		}	
		return $this->user_mapper;
	}
	/**
	 * global user login
	 * @var User $user
	 * return boolean
	 */ 
	function login(User $user){
		$mapper = $this->get_user_mapper();
		$now = TimeHelper::get_time_in_utc();		
		$user->last_login = $now;
		$user->last_update = $now;						
		return $mapper->update_last_login($user);		
	}
	/**
	 * local user login
	 * @var LoginForm $form
	 * return boolean
	 */ 
	public function local_user_login(LoginForm $form){
		$mapper = $this->get_user_mapper();
		$local_user = $mapper->get_local_user_by_username($form->username);
		if($local_user!=null){
			$bcrypt = new BcryptHelper(15);
			$isGood = $bcrypt->verify($form->password, $local_user->password);
			if($isGood){															
				if($this->login($local_user)){		
					$user = $mapper->get_user_by_id($local_user->userid);
					if($user==NULL){
						$this->add_error('username','user not found');
						return FALSE;
					}	
						
					FBTFactory::set_active_user($user);
					return TRUE;
				}else{
					$this->add_error('username','login failed');
				}			
			}else{
				$this->add_error('password','wrong password');
			}	
		}else{
			$this->add_error('username','user not found');
		}
		
		return FALSE;	
	}
}
