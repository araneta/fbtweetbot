<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use fbtweetbot\FBTFactory;
use fbtweetbot\Entities\User;
class MY_Controller extends CI_Controller {
	//current language: english, indonesia,..
	protected $m_current_lang;
	//data to be passed to view
	public $data = array();
	
    function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('language'))
		{
			$this->session->set_userdata('language','english');
		}
		$this->m_current_lang = $this->session->userdata('language');		
		FBTFactory::set_database($this->Gateway);
		
    }
	function bind($model){
		$data = array();
		foreach($model->cols as $col){
			$data[$col] = db_clean($this->input->post($col));
		}
		return $data;
	}
	function set_lang_file($file){
		$this->lang->load($file,$this->m_current_lang);
	}
	function need_login(){		
		if ($this->session->userdata('userid')==FALSE){			
			redirect('/','refresh');
			return;
		}
		//check expired date
		$this->load->model('user/MUser');
		$expired_date = $this->MUser->get_expired_date(intval($this->session->userdata('userid')));
		if($expired_date != null && strtotime($expired_date)<strtotime('now')){
			redirect('/p/expired','refresh');
			return;
		}
	}
	function is($role){		
		if ($this->session->userdata('role')==FALSE || $this->session->userdata('role')!=$role){			
			$this->session->set_flashdata('error','you are not an '.$role);			
			$this->logout();
			return;
		}		
	}
	function logout(){				
		$this->load->model('user/MActiveUser');
		$this->MActiveUser->logout();
		redirect('/','refresh');
	}		
	
	/*---------------*/
	function check_role($required_role){
		if ($this->session->userdata('userid')==FALSE ||
			$this->session->userdata('usertype')==FALSE){					
			return FALSE;
		}
		
		$user_role = $this->session->userdata('usertype');
		
		$process = FBTFactory::get_role_check_service();		
		$ret = $process->check($required_role,$user_role);
		return $ret;		
	}
	function required_role($role){
		if(!$this->check_role($role)){
			$this->session->set_flashdata('error','you dont have access');
			$this->unset_user();
			redirect('login','refresh');
			return FALSE;
		}
		
		return TRUE;
	}
	public function set_user(User $user){		
		$this->session->set_userdata('userid',$user->id);
		$this->session->set_userdata('usertype',$user->role);
		//$this->session->set_userdata('params',$user->parameters);		
		if(!empty($user->parameters)){
			$kv = json_decode($user->parameters,TRUE);			
			$this->add_param('user',$kv);
		}
	}
	public function unset_user(){
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('usertype');
		$this->session->unset_userdata('params');
	}
}

