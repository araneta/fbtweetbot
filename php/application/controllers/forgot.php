<?php
	class Forgot extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->set_lang_file('forgot');			
		}
		function index() {			
			$data['title'] = $this->lang->line('forgot');
			$data['main'] = 'forgot';		
			$this->load->view('template',$data);		
		}
		function submit()
		{			
			$this->load->model('user/MLocalUser');
			if ($this->input->post('email')){	
				$email = $this->input->post('email');							
				if ($this->MLocalUser->emailExists($email)){
					$this->load->model('user/MForgotPassword');
					$config = Array(
						'protocol' => 'smtp',
						'smtp_host' => 'ssl://smtp.googlemail.com',
						'smtp_port' => 465,
						'smtp_user' => 'aldopraherda@gmail.com',
						'smtp_pass' => 'sakurabiyorigedubrak',
						'mailtype'  => 'html', 
						'charset'   => 'iso-8859-1'
					);
					$this->load->library('email', $config);			
					$this->email->set_newline("\r\n");	
					$newpwd = $this->MLocalUser->reset($email);					
					$this->sendEmail($email,$newpwd);									
					$this->session->set_flashdata('info', $this->lang->line('sentinfo').' '.$email );

				}else
				{
					$this->session->set_flashdata('error',$this->lang->line('emailnotfound'));
				}
			}else
			{
				$this->session->set_flashdata('error',$this->lang->line('enteremail'));
			}
			redirect('forgot','refresh');
		}
		function sendEmail($email,$newpwd)
		{
			$subject = $this->lang->line('msgsubject');
			$msg = $this->lang->line('msgemail').$newpwd;
			
			$this->email->clear();
			$this->email->from("aldopraherda@gmail.com","support");
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->message($msg);
			if(!$this->email->send()){
				$this->email->print_debugger();
				exit(0);
			}
		}				
	}
?>
