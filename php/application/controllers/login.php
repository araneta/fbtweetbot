<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//git reset --hard HEAD
use fbtweetbot\FBTFactory;
use fbtweetbot\Entities\LoginForm;
class Login extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         
		$this->set_lang_file('login');
		//$this->load->model('user/MActiveUser');
		//if($this->MActiveUser->is_login()){
		if(FBTFactory::get_active_user()!=NULL){	
			redirect('user/dashboard');
			return;
		}				
	}
	public function index() {		
		$this->load->model('MLanguages');
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'login';		
		$this->data['langs'] = $this->MLanguages->get_all();
		
		//$this->load->model('user/MActiveUser');
		//if($this->MActiveUser->is_login()){
		//	redirect('user/dashboard');
		//	return;
		//}
		$user = FBTFactory::get_active_user();
		if($user!=NULL&& !empty($user->id)){
			redirect('user/dashboard');
			return;
		}       				
		$this->load->view('template',$this->data);		
	}

	public function verify() 
	{		
		$form = new LoginForm();
		$form->bind($_POST);
		if(!$form->validate()){			
			$this->session->set_flashdata('error',$form->error_messages());
			$this->session->set_flashdata('errorfields',json_encode($form->error_keys()));			
			redirect('/','refresh');	
		}else{									
			$process = FBTFactory::get_user_service();
			$status = $process->local_user_login($form);
			if($status){							
				$this->set_user(FBTFactory::get_active_user());
				redirect('user/dashboard');				
			}else
			{				
				$this->session->set_flashdata('error',$process->error_messages());
				$this->session->set_flashdata('errorfields',json_encode($process->error_keys()));
				redirect('/','refresh');	
			}
		} 	
	}
	public function fb(){
		//$this->load->model('user/MOAuthUser');
		//redirect($this->MOAuthUser->get_fb_login_url());
		redirect(FBTFactory::get_facebook_service()->get_login_url());
	}
	public function twitter(){
		$this->load->model('user/MOAuthUser');
		$this->MOAuthUser->get_twitter_login_url();
	}
}
?>
