<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	public function __construct() {
		parent::__construct();	         
		$this->set_lang_file('login');
					
	}
	public function index() {		
		$this->load->model('user/MActiveUser');
		if($this->MActiveUser->is_login()){
			redirect('user/dashboard');
			return;
		}	
		$this->data['title'] = 'Schedule tweets and facebook updates, It\'s Free';		
		$this->data['main'] = 'home';
		$this->data['cssfiles'] = array('nivo/themes/default/default.css','nivo/nivo-slider.css');
		$this->data['jsfiles'] = array('jquery.nivo.slider.pack.js','home.js');
		$this->load->view('template',$this->data);		
	}
	public function p($name){
		$routes = array();
		$routes['termsofuse'] = 'Terms of Use';
		$routes['privacy'] = 'Privacy Policy';
		$routes['expired'] = 'Expired Membership';	
		$routes['features'] = 'Features';
		$routes['expired'] =  'Expired Membership';
		$routes['schedule-tweets'] = 'Schedule Tweets';	
		$routes['tweet-queue']  = 'Tweet Queue';		
		$routes['schedule-facebook-accounts'] = 'Schedule for My Accounts';
		$routes['schedule-facebook-fans-pages'] = 'Schedule for My Pages';
		$routes['schedule-facebook-friends'] = 'Schedule for My Friends';	
		if(array_key_exists($name,$routes))
			$title = $routes[$name];
		else{
			redirect('/');
			return;
		}
		$this->m_data['title'] = $title;
		$this->m_data['main'] = $name;
		$this->load->view('templatedisqus',$this->m_data);	
	}
	
}


