<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//images controller
class Uploaddir extends MY_Controller{
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
	}
	public function _remap($method, $params = array()){
		$this->view($method);
	}
	//function view($dir,$fname){
	function view($fname){	
		//$image =  $this->config->item('upload_dir').'/'.$dir.'/'.$fname; 
		$image =  $this->config->item('upload_dir').'/'.$fname; 
		if(realpath($image)){
			$content = file_get_contents($image); 
			header('Content-Type: image/jpeg');
			echo $content; 
			exit();
		}else
		{
			echo 'not found '.$image;
		}
	}
}
