<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MY_Controller
{
	function __construct()
	{
		parent::__construct();		
		//$this->set_lang_file('admin/dashboard');		
		$this->need_login();	
		$this->is('admin');    
	}
	function index()
	{				 		 		
		$data['title'] = $this->lang->line('title');		
		$data['main'] = 'admin/user_home';			
		$this->load->view('admin/dashboard',$data);				
	}
	function logout(){
		parent::logout();
	}		
}
?>
