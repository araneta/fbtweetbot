<?php
	class Profile extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();
			$this->set_lang_file('user/profile');
			
			$this->load->model('user/MLocalUser');
			$this->load->model('user/MActiveUser');
		}
		function index()
		{						
			$this->load->helper('timezone');						
			$user = $this->MActiveUser->get_user();													
			
			$data['user'] = $user;
			$data['title'] = $this->lang->line('profiletitle');		
			$data['main'] = 'user/profile';			
			$this->load->view('user/dashboard',$data);	
		}		
		function email_check($email)
		{				
			if($this->MLocalUser->isUserEmail(
					$email,$this->MActiveUser->get_local_user_id()
				)
			)
			{				
				return TRUE;
			}else
			{
				if($this->MLocalUser->emailExists($email))
				{
					$this->form_validation->set_message('email_check',
						$this->lang->line('emailexists'));
					return FALSE;
				}
			}			
		}
		
		function submitValidate()
		{			
			$this->form_validation->set_rules('password', 'lang:password', 'required');
			$this->form_validation->set_rules('email', 'lang:email', 'required|valid_email|callback_email_check');
			$this->form_validation->set_rules('timezone', 'lang:Timezone', 'required');
			return ($this->form_validation->run());
		}
		
		function save()
		{
			
			if($this->submitValidate()==FALSE)
			{
				$this->index();
				return;
			}
			else         
			{						
				$this->MLocalUser->save($this->bind($this->MLocalUser));					
				$this->session->set_flashdata('info',$this->lang->line('success'));
				redirect('user/profile','refresh');
			}		
		}
	}
?>
