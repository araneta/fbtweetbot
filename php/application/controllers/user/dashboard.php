<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends MY_Controller
{
	function __construct()
	{
		parent::__construct();		
		$this->set_lang_file('user/dashboard');		
		//$this->need_login();	    
		$this->required_role('member');
	}
	function index()
	{				 		 		
		$data['title'] = $this->lang->line('title');		
		$data['main'] = 'user/user_home';		
		$data['cssfiles'] = array('jquery.stepy.css');	
		$data['jsfiles'] = array('jquery.stepy.min.js');	
		$this->load->view('user/dashboard',$data);				
	}
	function logout(){
		parent::logout();
	}		
}
?>
