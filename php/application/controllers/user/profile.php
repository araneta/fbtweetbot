<?php
	class Profile extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();
			$this->set_lang_file('user/profile');
						
		}
		function index()
		{						
			$this->load->model('user/MActiveUser');								
			$user = $this->MActiveUser->get_user();
			switch($user['user_type']){				
				case 2:
					redirect('user/oauthprofile');			
				break;
			}	
			$this->load->helper('timezone');
			$data['main'] = 'user/profile';
			$data['title'] = $this->lang->line('profiletitle');			
			$data['user'] = $user;
			$this->load->view('user/dashboard',$data);	
		}		
		function email_check($email)
		{				
			$this->load->model('user/MActiveUser');
			if($this->MLocalUser->isUserEmail($email,$this->MActiveUser->get_local_user_id()))
			{				
				return TRUE;
			}else
			{
				if($this->MLocalUser->emailExists($email))
				{
					$this->form_validation->set_message('email_check',$this->lang->line('emailexists'));
					return FALSE;
				}
			}			
		}
		
		function submitValidate()
		{						
			$this->form_validation->set_rules('email', 'lang:email', 'required|valid_email|callback_email_check');
			$this->form_validation->set_rules('timezone', 'lang:Timezone', 'required');
			return ($this->form_validation->run());
		}
		
		function save()
		{
			$this->load->model('user/MLocalUser');
			if($this->submitValidate()==FALSE)
			{
				$this->index();
				return;
			}
			else         
			{						
				$user = $this->bind($this->MLocalUser);
				$user['timezone'] = $this->input->post('timezone');
				$this->MLocalUser->update_profile($user);					
				$this->session->set_flashdata('info',$this->lang->line('success'));
				redirect('user/profile','refresh');
			}		
		}
		/*change password*/
		function newpassword_check($pass){
			if(empty($pass))
				return FALSE;
			if($this->input->post('newpassword')!=$this->input->post('renewpassword')){
				$this->form_validation->set_message('newpassword_check',$this->lang->line('passmissmatch'));
				return FALSE;
			}
			return TRUE;
		}
		function submitPassValidate()
		{						
			$this->form_validation->set_rules('currentpassword', 'lang:currentpassword', 'required');
			$this->form_validation->set_rules('newpassword', 'lang:newpassword', 'required');
			$this->form_validation->set_rules('renewpassword', 'lang:renewpassword', 'required|callback_newpassword_check');
			return ($this->form_validation->run());
		}
		function changepassword(){
			
			if($this->submitPassValidate()==FALSE)
			{
				$this->index();
				return;
			}
			else         
			{
				$this->load->model('user/MLocalUser');										
				if($this->MLocalUser->change_password(
					$this->input->post('username')
					,$this->input->post('currentpassword')
					,$this->input->post('newpassword')
				)){				
					$this->session->set_flashdata('info',$this->lang->line('changepasssuccess'));
				}else{
					$this->session->set_flashdata('info',$this->lang->line('changepassfailed'));
				}
				redirect('user/profile','refresh');
			}		
		}
	}
?>
