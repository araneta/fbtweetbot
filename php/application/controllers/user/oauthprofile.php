<?php

	class OAuthProfile extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();
			$this->set_lang_file('user/profile');
						
		}
		function index()
		{						
			$this->load->model('user/MActiveUser');								
			$user = $this->MActiveUser->get_user();			
			$this->load->helper('timezone');
			$data['main'] = 'user/oauthprofile';
			$data['title'] = $this->lang->line('profiletitle');			
			$data['user'] = $user;
			$this->load->view('user/dashboard',$data);	
		}		
		
		function submitValidate()
		{									
			$this->form_validation->set_rules('timezone', 'lang:Timezone', 'required');
			return ($this->form_validation->run());
		}
		
		function save()
		{
			$this->load->model('user/MOauthUser');
			if($this->submitValidate()==FALSE)
			{
				$this->index();
				return;
			}
			else         
			{						
				$user = $this->bind($this->MOauthUser);
				$user['timezone'] = $this->input->post('timezone');
				$this->MOauthUser->update_profile($user);					
				$this->session->set_flashdata('info',$this->lang->line('success'));
				redirect('user/oauthprofile','refresh');
			}		
		}
	}
?>
