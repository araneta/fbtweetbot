<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php $this->load->view('head');?>  
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/style.css" />      
    </head>
    <body>
        <div id="page-wrap">
			<div id="header">
				<?php $this->load->view('header');?>
			</div>
			<?php $this->load->view("user/user_header");?>
			<div id="usercontent">
				<div id="panel">
					<h2>
						<span class="title"><?php echo $title;?></span>
					</h2>
					<hr class="hrgrey" />
					<div id="status">
						<?php $this->load->view('status'); ?>
					</div>
					<?php $this->load->view($main);?>		
				</div>	
				<div class="clear"></div>
			</div>
			<div id="footer">
				<?php $this->load->view('footer');?>
			</div>
		</div>
		<?php
            if(isset($jslang))
            {
            ?>
            <script type="text/javascript" src="<?php echo base_url();?>javalang/f/<?php echo $jslang; ?>"></script>
            <?php
            }
            if (isset($jsfiles) && count($jsfiles)){		
                foreach ($jsfiles as $js){
                    echo "<script type=\"text/javascript\" src=\"".base_url()."js/$js\"></script>\r\n";			
                }		
            } 	
        ?>
     <?php $this->load->view('endbody');?>  
	</body>
</html>		

