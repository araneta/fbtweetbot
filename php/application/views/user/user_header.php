<div id="navigation">
    <ul>
        <li><a class="current" href="<?php echo  base_url();?>" target="_self">Home</a></li>
        <li><a href="<?php echo base_url('accounts/index');?>" title="Account List">Account List<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="#" onclick="return false;">Twitter<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul>
                <li><a href="<?php echo base_url('tweetscheduler/index');?>" title="Schedule Tweets">Schedule Tweets</a></li>
                <li><a href="<?php echo base_url('tweetqueue/index');?>" title="Tweet Queue">Tweet Queue</a></li>
                
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="#" onclick="return false;">Facebook<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul>
                <li><a href="<?php echo base_url('fbpages/index');?>" title="Facebook Page List">Facebook Page List</a></li>
                <li><a href="<?php echo base_url('fbscheduler/index');?>" title="Schedule for my accounts">Schedule for my accounts</a></li>
                <li><a href="<?php echo base_url('fbpagescheduler/index');?>" title="Schedule for my pages">Schedule for my pages</a></li>
                <li><a href="<?php echo base_url('fbfriendscheduler/index');?>" title="Schedule for my friends">Schedule for my friends</a></li>                
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="#" onclick="return false;">Help<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul>
				<li>
					<a href="#" onclick="return false;"  title="Twitter">Twitter</a>
					<ul>
						<li>
							<a target="_blank" href="<?php echo base_url('p/schedule-tweets');?>">Schedule Tweets</a>
						</li>
						<li>
							<a target="_blank" href="<?php echo base_url('p/tweet-queue');?>">Tweet Queue</a>
						</li>
					</ul>
				</li> 
				
                <li>
					<a href="#" onclick="return false;" title="Facebook">Facebook</a>
					<ul>
						<li>
							<a href="<?php echo base_url('p/schedule-facebook-accounts');?>" target="_blank">Schedule for my accounts</a>
						</li>
						<li>
							<a href="<?php echo base_url('p/schedule-facebook-fans-pages');?>" target="_blank">Schedule for My Fans Pages</a>
						</li>
						<li>
							<a href="<?php echo base_url('p/schedule-facebook-friends');?>" target="_blank">Schedule for My Friends</a>
						</li>
					</ul>
				</li>
                         
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>               
    </ul>
</div>
