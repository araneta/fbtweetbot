<div class="form">
		<?php echo form_open('user/profile/save'); ?>
        <?php
            if ($this->session->flashdata('message')){
                echo "<div class='message'>";
                echo $this->session->flashdata('message');
                echo "</div>";
            }	
        ?>
       
        <fieldset>
			<dl>
				<dt><label><?php echo $this->lang->line('username');?></label></dt>
				<dd>						
                    <?php echo form_input('username',$user['username'],'readonly="readonly"'); ?>	
                    <?php echo form_hidden('userid',$user['userid']); ?>
                    <?php echo form_hidden('id',$user['id']); ?>
				</dd>
			</dl>			
			<dl>
				<dt><label><?php echo $this->lang->line('email');?></label></dt>
				<dd><?php echo form_input('email',$user['email']); ?></dd>
			</dl>
			<dl>
				<dt><?php echo $this->lang->line('timezone');?></dt>
				<dd><?php get_tz_options('timezone',set_value('timezone',$user['timezone'])); ?></dd>
			</dl>
			<dl>
				<dt>&nbsp;</dt>
				<dd>
					<input type="submit" value="<?php echo $this->lang->line('save');?>" />
					<?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('/').'\'"'); ?>
				</dd>
			</dl>
			
		 <fieldset>
        
        <?php echo form_close();?>
</div>
<div style="width:100%;height:40px;" class="clear"></div>
<h2>
	<span class="title"><?php echo $this->lang->line('changepassword');?></span>
</h2>
<div class="form">
		<?php echo form_open('user/profile/changepassword'); ?>
        <?php
            if ($this->session->flashdata('message')){
                echo "<div class='message'>";
                echo $this->session->flashdata('message');
                echo "</div>";
            }	
        ?>
       
        <fieldset>
        	<dl>
        	 	<?php echo form_hidden('username',$user['username']); ?>
				<dt><?php echo $this->lang->line('currentpassword');?></dt>
				<dd><?php echo form_password('currentpassword'); ?></dd>
			</dl>
			<dl>
				<dt><?php echo $this->lang->line('newpassword');?></dt>
				<dd><?php echo form_password('newpassword'); ?></dd>
			</dl>
			<dl>
				<dt><?php echo $this->lang->line('renewpassword');?></dt>
				<dd><?php echo form_password('renewpassword'); ?></dd>
			</dl>
			<dl>
				<dt>&nbsp;</dt>
				<dd>
					<input type="submit" value="<?php echo $this->lang->line('save');?>" />
					<?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('/').'\'"'); ?>
				</dd>
			</dl>
        </fieldset>
</div>
