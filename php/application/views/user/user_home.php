<form id="default">
	<fieldset title="Step 1">
		<legend>Register your social accounts</legend>
		<div class="desc">
			Go to <a href="<?php echo base_url('accounts/index');?>">Account List</a> menu then click <a href="<?php echo base_url('accounts/addtwitter');?>">Add Twitter Account</a> or <a href="<?php echo base_url('accounts/addfacebook');?>">Add Facebook Account</a> to add your accounts into fbtweetbot database. 
			If you want to post messages to your Facebook pages then you also need to register your facebook pages. 
			To register your facebook pages please go to menu: Facebook > <a href="<?php echo base_url('fbpages/index');?>">Facebook Page List</a> then click <a href="<?php echo base_url('fbpages/add');?>">Add Facebook Pages</a>
		</div>
		<img src="<?php echo base_url('images/accountlist.png');?>" />
		
	</fieldset>

	<fieldset title="Step 2">
		<legend>Create schedules</legend>		
		<div class="desc"> 
			After you added your accounts into Fbtweetbot then you can create schedules for your tweets and facebook updates. 			
			You can schedule tweets using <a href="<?php echo base_url('tweetscheduler/index');?>">Simple Scheduler</a> or <a href="<?php echo base_url('tweetqueue/index');?>">Queue</a>.
			you can also schedule Facebook updates for <a href="<?php echo base_url('fbscheduler/index');?>">your accounts</a>, <a href="<?php echo base_url('fbpagescheduler/index');?>">your pages</a>, or post it to <a href="<?php echo base_url('fbfriendscheduler/index');?>">your friends wall</a>.
			Fbtweetbot allows you to share images or links to your messages easily, even in the different timezone. You can change the default timezone from <a href="<?php echo base_url('user/profile');?>">Profile</a> page.
		</div>
		<img src="<?php echo base_url('images/scheduletweets.png');?>" />
	</fieldset>

	<input type="submit" class="finish" value="Finish!" />
</form>
<script type="text/javascript">
	$(function() {
		$('#default').stepy();				
	});
</script>
