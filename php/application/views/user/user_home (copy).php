<ul id="dashboardmenu">
<li>
	<b><?php echo anchor("user/profile",$this->lang->line('manageprofile'));?>.</b>
	<br/>
	Update email, timezone and password
	<br/>
</li>
<li>
	<b><?php echo anchor("accounts/index","Account List");?>.</b>
	<br/>
	Manage facebook and twitter accounts
	<br/>
</li>

<li>
	<b><?php echo anchor("tweetscheduler/index","Schedule tweets");?>.</b>
	<br/>
	Post tweets at specific time
	<br/>
</li>

<li>
	<b><?php echo anchor("tweetqueue/index","Tweet Queue");?>.</b>
	<br/>
	Schedule tweets on a queue within a user-defined time interval
	<br/>
</li>

<li>
	<b><?php echo anchor("fbpages/index","FB Page List");?>.</b>
	<br/>
	Manage Facebook Pages
	<br/>
</li>

<li>
	<b><?php echo anchor("fbscheduler/index","Schedule Facebook Updates for my accounts");?>.</b>
	<br/>
	Schedule Facebook updates for Facebook accounts
	<br/>
</li>
<li>
	<b><?php echo anchor("fbpagescheduler/index","Schedule Facebook Updates for my pages");?>.</b>
	<br/>
	Schedule Facebook updates for Facebook Page accounts
	<br/>
</li>
<li>
	<b><?php echo anchor("fbpagescheduler/index","Schedule Facebook Updates for my friends");?>.</b>
	<br/>
	Schedule Facebook updates for any Facebook accounts
	<br/>
</li>
</ul>
