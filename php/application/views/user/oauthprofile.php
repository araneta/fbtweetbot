<div class="form">
            <?php echo form_open('user/oauthprofile/save'); ?>
        <?php
            if ($this->session->flashdata('message')){
                echo "<div class='message'>";
                echo $this->session->flashdata('message');
                echo "</div>";
            }	
        ?>
       
        <fieldset>
			<dl>
				<dt><label><?php echo $this->lang->line('username');?></label></dt>
				<dd>						
                    <?php echo form_input('oauth_username',$user['oauth_username'],'readonly="readonly"'); ?>	
                    <?php echo form_hidden('userid',$user['userid']); ?>
                    <?php echo form_hidden('id',$user['id']); ?>
				</dd>
			</dl>
			<dl>
				<dt><label><?php echo $this->lang->line('oauth_provider');?></label></dt>
				<dd>						
                    <?php echo form_input('oauth_provider',$user['oauth_provider'],'readonly="readonly"'); ?>	                    
				</dd>
			</dl>						
			<dl>
				<dt><?php echo $this->lang->line('timezone');?></dt>
				<dd><?php get_tz_options('timezone',set_value('timezone',$user['timezone'])); ?></dd>
			</dl>
			<dl>
				<dt>&nbsp;</dt>
				<dd>
					<input type="submit" value="<?php echo $this->lang->line('save');?>" />
					<?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('/').'\'"'); ?>
				</dd>
			</dl>
			<dl>
				<dt></dt>
				<dd></dd>
			</dl>
		 <fieldset>
        
        <?php echo form_close();?>
</div>
