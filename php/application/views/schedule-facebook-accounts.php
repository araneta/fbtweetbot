<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>
		<li><a href="<?php echo base_url('p/scheduleformyaccounts'); ?>">Facebook : Schedule for My Account</a></li>
	</ul>
	<h1>Schedule for My Account</h1>
	<p><a href="<?php echo base_url(); ?>">Fbtweetbot.com</a> allows you to schedule facebook status updates at specific time easily. To create a schedule for your facebook accounts please follow these steps:</p>
	<ol class="digit">
		<li>Go to menu Facebook &gt; Schedule for My Accounts </li>
		<li>In Schedule for My Accounts page click Add Schedule button to create a new schedule<br />
			<img border="1" src="../images/scheduleupdatestatus.png" alt="Schedule Facebook status updates" />
		</li>	
		<li>In Add Schedule for My Account page please select your facebook username,<br />
		 enter the date, time and timezone when the message will be posted,<br />
		 enter the message, you can also upload an image with your message or share a link<br />	 
		 <img border="1" src="../images/schedulefacebookupdatesentry.png" alt="Schedule Facebook status updates entry" />
		</li>
		<li>Click Save button to save the schedule</li>
	</ol>
</div>
