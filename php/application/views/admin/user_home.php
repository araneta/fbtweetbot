<ul>
<li>
	<b><?php echo anchor("user/profile",$this->lang->line('manageprofile'));?>.</b>
	<br/>
	<?php echo $this->lang->line('manageprofiledesc'); ?>
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/accounts/index","Account List");?>.</b>
	<br/>
	List account
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/tweetscheduler/index","Schedule tweet");?>.</b>
	<br/>
	Schedule tweet 
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/tweetqueue/index","Tweet Queue");?>.</b>
	<br/>
	Tweet Queue
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/fbpages/index","FB Page List");?>.</b>
	<br/>
	FB Page List
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/fbscheduler/index","Schedule FB Update Status (User)");?>.</b>
	<br/>
	Schedule FB Update Status (User)
	<br/>
</li>
<br/>
<li>
	<b><?php echo anchor("admin/fbpagescheduler/index","Schedule FB Update Status (Page)");?>.</b>
	<br/>
	Schedule FB Update Status (Page)
	<br/>
</li>
<br/>
</ul>
