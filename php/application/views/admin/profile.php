<div class="form">
            <?php echo form_open('user/profile/save'); ?>
        <?php
            if ($this->session->flashdata('message')){
                echo "<div class='message'>";
                echo $this->session->flashdata('message');
                echo "</div>";
            }	
        ?>
       
        <fieldset>
			<dl>
				<dt><label><?php echo $this->lang->line('username');?></label></dt>
				<dd>						
                    <?php echo form_input('username',$user['username'],'readonly="readonly"'); ?>	
                    <?php echo form_hidden('id',$user['id']); ?>
				</dd>
			</dl>
			<dl>
				<dt><label><?php echo $this->lang->line('password');?></label></dt>
				<dd><?php echo form_password('password'); ?></dd>
			</dl>
			<dl>
				<dt><label><?php echo $this->lang->line('confirmpassword');?></label></dt>
				<dd><?php echo form_password('password2'); ?>	</dd>
			</dl>
			<dl>
				<dt><label><?php echo $this->lang->line('email');?></label></dt>
				<dd><?php echo form_input('email',$user['email']); ?></dd>
			</dl>
			<dl>
				<dt><?php echo $this->lang->line('timezone');?></dt>
				<dd><?php get_tz_options('timezone',set_value('timezone',$user['timezone'])); ?></dd>
			</dl>
			<dl>
				<dt>&nbsp;</dt>
				<dd>
					<input type="submit" value="<?php echo $this->lang->line('save');?>" />
					<?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('/').'\'"'); ?>
				</dd>
			</dl>
			<dl>
				<dt></dt>
				<dd></dd>
			</dl>
		 <fieldset>
        
        <?php echo form_close();?>
</div>
