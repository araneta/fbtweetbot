<div id="navigation">
    <ul>
        <li><a class="current" href="<?php echo  base_url();?>" target="_self">Home</a></li>
        <li><a href="<?php echo base_url('admin/accounts/index');?>" title="Account List">Account List<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="#" onclick="return false;">Twitter<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul>
                <li><a href="<?php echo base_url('admin/tweetscheduler/index');?>" title="Schedule Tweets">Schedule Tweets</a></li>
                <li><a href="<?php echo base_url('admin/tweetqueue/index');?>" title="Tweet Queue">Tweet Queue</a></li>
                
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="#" onclick="return false;">Facebook<!--[if IE 7]><!--></a><!--<![endif]-->
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <ul>
                <li><a href="<?php echo base_url('admin/fbpages/index');?>" title="Facebook Page List">Facebook Page List</a></li>
                <li><a href="<?php echo base_url('admin/fbscheduler/index');?>" title="Schedule for my accounts">Schedule for my accounts</a></li>
                <li><a href="<?php echo base_url('admin/fbfriendscheduler/index');?>" title="Schedule for my pages">Schedule for my pages</a></li>
                <li><a href="<?php echo base_url('admin/fbpagescheduler/index');?>" title="Schedule for my friends">Schedule for my friends</a></li>                
            </ul>
            <!--[if lte IE 6]></td></tr></table></a><![endif]-->
        </li>
        <li><a href="help">Help<!--[if IE 7]><!--></a><!--<![endif]-->                   
    </ul>
</div>
