<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>
		<li><a href="<?php echo base_url('p/scheduleformypages'); ?>">Facebook : Schedule for My Pages</a></li>
	</ul>
	<h1>Schedule for My Fans Page</h1>
	<p><a href="<?php echo base_url(); ?>">Fbtweetbot.com</a> allows you to schedule facebook status updates at specific time easily. To create a schedule for your facebook fans pages please follow these steps:</p>
	<ol class="digit">
		<li>If you have not registered your facebook fans pages to fbtweetbot then you need to go to menu Facebook &gt; Facebook Page List and click Add Facebook Pages to register your Facebook fans pages<br />
			<img border="1" src="../images/facebookpagelist.png" alt="Facebook Fans Pages" />
		</li>
		<li>Go to menu Facebook &gt; Schedule for My Pages </li>
		<li>In Schedule for My Pages page click Add Schedule button to create a new schedule<br />
			<img border="1" src="../images/schedulepageupdates.png" alt="Schedule Facebook fans page status updates" />
		</li>	
		<li>In Add Schedule for My Pages page please select your facebook fans page,<br />
		 enter the date, time and timezone when the message will be posted,<br />
		 enter the message, you can also upload an image with your message or share a link<br />	 
		 <img border="1" src="../images/schedulepageupdatesentry.png" alt="Schedule Facebook fans page status updates entry" />
		</li>
		<li>Click Save button to save the schedule</li>
	</ol>
</div>
