<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>
		<li><a href="<?php echo base_url('p/scheduletweets'); ?>">Schedule Tweets</a></li>
	</ul>
	<h1>Schedule Tweets</h1>
	<p><a href="<?php echo base_url(); ?>">Fbtweetbot.com</a> allows you to schedule tweets at specific time easily. To create a schedule for your tweet please follow these steps:</p>
	<ol class="digit">
		<li>Go to menu Twitter &gt; Schedule Tweets </li>
		<li>In Schedule Tweets page click Add Schedule button to create a new schedule<br />
			<img border="1" src="../images/scheduletweetslist.png" alt="Schedule Tweet List" />
		</li>	
		<li>In Schedule Tweets entry page please select your twitter username,<br />
		 enter the date, time and timezone when the message will be posted,<br />
		 enter the message, you can click tiny links to shorten the url in the message<br />
		 you can even share an image with your tweet.<br />
		 <img border="1" src="../images/scheduletweetsentry.png" alt="Schedule Tweet Entry" />
		</li>
		<li>Click Save button to save the schedule</li>
	</ol>
</div>
