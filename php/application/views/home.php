<div class="feature">					
	<div class="slider-wrapper theme-default">
		<div id="slider" class="nivoSlider">
			<img src="<?php echo base_url('images/home/accountlist.png');?>" alt="" title="One application to schedule messages for your facebook and twitter account" />
			<img src="<?php echo base_url('images/home/scheduletweetsentry.png');?>" alt="" title="Fbtweetbot allows you to post messages in different timezone easily" />							
			<img src="<?php echo base_url('images/home/postimageorlink.png');?>" alt="" title="Post messages to facebook and twitter with an image or link. No Problem!" />							
			<img src="<?php echo base_url('images/home/schedulefacebookupdatesentry.png');?>" alt="" title="Post messages to your facebook account, your community page account or your friends wall" />							
			<img src="<?php echo base_url('images/home/tweetqueueadd.png');?>" alt="" title="Fbtweetbot allows you to create queues for your tweets easily" />							
		</div>						
	</div>					
	<div class="feature-text">
		<ul>
			<li>Schedule tweets and facebook updates in one application, and it's free</li>
			<li>Free twitter marketing tool</li>
			<li>Complete Facebook Campaigns tool</li>
			<li>Fbtweetbot allows you to post messages in different timezone easily</li>
			<li>Post messages to facebook and twitter with an image or link. No Problem!</li>
			<li>Post messages to your facebook account, your community page account or your friends wall</li>
			<li>Fbtweetbot allows you to create queues for your tweets easily</li>
			<a href="<?php echo base_url('p/features');?>">Read More...</a>
		</ul>						
	</div>
</div>
<div id="login">
	<div id="loginbox">			
		<div id="status">
			<?php $this->load->view('status'); ?>
		</div>
		<!--
		<?php echo $this->security->get_csrf_token_name();?>:<?php echo $this->security->get_csrf_hash();?>						
		-->
		<?php echo form_open('login/verify'); ?>  
			<div class="fieldbox">
				<label><?php echo $this->lang->line('username');?></label>
				<?php echo form_input('username',set_value('username'),'class="input"'); ?>
			</div>
			<div class="fieldbox">
				<label><?php echo $this->lang->line('password');?></label>
				<?php echo form_password('password',set_value('password'),'class="input"'); ?>
			</div>
			<div class="fieldbox">
				<?php echo form_submit('submit',$this->lang->line('login'),'class="button login"'); ?>																	
			</div>	
			<div id="sociallogin">
				<a href="<?php echo base_url('login/fb');?>" id="fblogin">Login with Facebook</a>
				<a href="<?php echo base_url('login/twitter');?>" id="twitterlogin">Login with Twitter</a>
				<a id="forgot" href="<?php echo base_url('forgot');?>">Forgot password</a>
			</div>
		 <?php echo form_close(); ?>
	</div>
</div>
				
			
