<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>
		<li><a href="<?php echo base_url('p/tweetqueue'); ?>">Tweet Queue</a></li>
	</ul>
	<h1>Tweet Queue</h1>
	<p><a href="<?php echo base_url(); ?>">Fbtweetbot.com</a> allows you to create a queue for your tweets, it's useful for sending bulk tweet with a specific interval. To create a queue for your tweet please follow these steps:</p>
	 
	<ol class="digit">
		<li>Go to menu Twitter &gt; Tweet Queue </li>
		<li>In Tweet Queue page click Add Queue button to create a new queue<br />
			<img border="1" src="../images/tweetqueuelist.png" alt="Tweet Queue List" />
		</li>	
		<li>In Tweet Queue Entry page please enter queue name, select your twitter username,<br />
		 enter the date, time and timezone when the message will be posted for first time,<br />
		 enter the interval between tweets<br />	 
		 <img border="1" src="../images/tweetqueueadd.png" alt="Tweet Queue Entry List" />
		</li>
		<li>Click Save button to save the queue</li>
		<li>After the queue saved, you will be redirected to Tweet Queue page again, right now please click the Message icon in the table to enter the messages<br />
		<img border="1" src="../images/tweetqueuelist2.png" alt="Tweet Queue List" />
		</li>
		<li>In Tweet Queue : Add a Message page, please enter the message, you can click tiny links to shorten the url in the message and you can share an image with your tweet.<br />
		You can add as many messages as you want with different Scheduled Time.
		 <img border="1" src="../images/tweetqueueaddmessage.png" alt="Tweet Queue, Message Entry" />
		</li>
	</ol>
</div>
