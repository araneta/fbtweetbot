<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php $this->load->view('head');?>
    </head>
    <body>
        <div id="page-wrap">
			<div id="header">
				<?php $this->load->view('header');?>
			</div>
			<div id="content">
				<?php $this->load->view($main);?>				
				<div class="clear"></div>
			</div>
			<div id="footer">
				<?php $this->load->view('footer');?>
			</div>
		</div>
		<?php $this->load->view('endbody');?> 
	</body>
</html>		

