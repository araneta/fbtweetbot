<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>		
	</ul>
	<h1>Features</h1>
	<br />
	<h2>Twitter</h2>
	<ol class="digit">
	<li><a href="<?php echo base_url('p/schedule-tweets');?>">Schedule Tweets</a></li>
	<li><a href="<?php echo base_url('p/tweet-queue');?>">Tweet Queue</a></li>
	</ol>
	<br />
	<h2>Facebook</h2>
	<ol class="digit">
	<li><a href="<?php echo base_url('p/schedule-facebook-accounts');?>">Schedule for My Accounts</a></li>
	<li><a href="<?php echo base_url('p/schedule-facebook-fans-pages');?>">Schedule for My Fans Pages</a></li>
	<li><a href="<?php echo base_url('p/schedule-facebook-friends');?>">Schedule for My Friends</a></li>
	</ol>
</div>
