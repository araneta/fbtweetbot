<div class="signup_form">
    <h2>Sign Up</h2>    
	<div id="status">
		<?php $this->load->view('status'); ?>
	</div>  
    <?php echo form_open('register/create'); ?>
    <fieldset>
        <dl>
            <dt><label><?php echo $this->lang->line('username');?></label></dt>
            <dd><?php echo form_input('username',set_value('username'),'class="input"'); ?></dd>
        </dl>
        <dl>
            <dt><label><?php echo $this->lang->line('password');?></label></dt>
            <dd><?php echo form_password('password',set_value('password'),'class="input"'); ?></dd>
        </dl>
        <dl>
            <dt><label><?php echo $this->lang->line('confirmpassword');?></label></dt>
            <dd><?php echo form_password('password2',set_value('password2'),'class="input"'); ?></dd>
        </dl>
        <dl>
            <dt><label><?php echo $this->lang->line('email');?></label></dt>
            <dd><?php echo form_input('email',set_value('email'),'class="input"'); ?></dd>
        </dl>
        <dl>
			<dt><label>Time Zone:</label></dt>
			<dd><?php get_tz_options('timezone',set_value('timezone')); ?></dd>
		</dl>
        <dl>
            <dt><label><?php echo $this->lang->line('captcha');?></label></dt>
            <dd><?php echo $recaptcha; ?></dd>
        </dl>    
        <dl>
			<dt>&nbsp;</dt>
			<dd><input type="submit" value="<?php echo $this->lang->line('signup');?>" class="button" /></dd>
		</dl>
    </fieldset>
    <?php echo form_close();?>
</div>
