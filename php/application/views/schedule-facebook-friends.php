<div id="inner">
	<ul id="breadcrumb">
		<li><a href="<?php echo base_url(); ?>">Home</a></li>
		<li><a href="<?php echo base_url('p/features'); ?>">Features</a></li>
		<li><a href="<?php echo base_url('p/scheduleformyfriends'); ?>">Facebook : Schedule for My Friends</a></li>
	</ul>
	<h1>Schedule for My Friends</h1>
	<p><a href="<?php echo base_url(); ?>">Fbtweetbot.com</a> allows you to schedule facebook status updates at specific time easily. To create a schedule for your facebook friends please follow these steps:</p>
	<ol class="digit">
		<li>Go to menu Facebook &gt; Schedule for My Friends </li>
		<li>In Schedule for My Friends page click Add Schedule button to create a new schedule<br />
			<img border="1" src="../images/schedulefriendupdates.png" alt="Schedule Facebook Updates for my friends" />
		</li>	
		<li>In Add Schedule for My Friend page please select your facebook account as a sender, and enter your friend account<br />
		 enter the date, time and timezone when the message will be posted,<br />
		 enter the message, you can also upload an image with your message or share a link<br />	 
		 <img border="1" src="../images/schedulefriendupdatesentry.png" alt="Schedule Facebook fans page status updates entry" />
		</li>
		<li>Click Save button to save the schedule</li>
	</ol>
</div>
