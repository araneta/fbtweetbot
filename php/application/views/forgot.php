<div class="signup_form">
    <h3><?php echo $this->lang->line('forgottitle');?></h3>
    <?php echo form_open('forgot/submit'); ?>
    <fieldset>
        <div id="status">
			<?php $this->load->view('status'); ?>
		</div>  
        <dl>
            <dt><label for="email"><?php echo $this->lang->line('email');?></label></dt>
            <dd><?php echo form_input('email',set_value('email'),'class="input"'); ?></dd>
        </dl>	
        <dl class="submit">
            <?php echo form_submit('submit',$this->lang->line('send'),'class="button"'); ?>
        </dl>
    </fieldset>
    <?php echo form_close(); ?>
</div>
