<?php

require_once dirname(__FILE__).'/muser.php';
class MLocalUser extends MUser{
	public $m_table = 'local_user';
	public $user_type = 1;
	public $cols = array(
		'id'
		,'username'
		,'password'
		,'email'
		,'created_date'		
		,'last_update'		
		,'userid'
		);
	function __construct()
	{		
		parent::__construct();
		
	}

	function register($user)
	{
		$this->load->helper('bcrypt');
		$bcrypt = new Bcrypt(15);		
		$user['password'] = $bcrypt->hash($user['password']);		
		$now = date("Y-m-d H:i:s");															
		$user['created_date'] = $now;					
		$user['last_update'] = $now;
			
		$ret= true;
		$this->db->trans_begin();
		
		if(parent::register($user)==FALSE){				
			$this->add_error('failed saving global user');
			$ret = false;				
		}
		else
		{						
			$user['userid'] = $user['id'];
			unset($user['id']);					
			if($this->save($user)==FALSE){
				echo('failed saving user');
				$ret = false;
			}			
		}
		
		if ($this->db->trans_status() === FALSE || $ret == false)
		{
			$this->db->trans_rollback();
			echo('Transaction failed');			
			$ret = false;			
		} 
		else
		{
			$this->db->trans_commit();
		}		
		return $ret;
	}
	
	function usernameExists($username)
	{
		$user = $this->get_by(array('username'=>db_clean($username)));
		if($user==null)
			return false;
		return true;
	}
	function emailExists($email)
	{
		$user = $this->get_by(array('email'=>db_clean($email)));
		if($user==null)
			return false;
		
		return true;
	}	
	function isUserEmail($email,$userid)
	{		
		$users = $this->get_by(array('email'=>db_clean($email),'id'=>id_clean($userid)),0,1);
		if(sizeof($users)==0)
		{			
			return FALSE;
		}
		return TRUE;
	}
	function verifyUser($u,$pw)
	{
		$users = $this->get_by(array('username'=>db_clean($u)),0,1);
		
		if($users!=null){
			$user= $users[0];
			$this->load->helper('bcrypt');
			$bcrypt = new MY_Bcrypt(15);
			$isGood = $bcrypt->verify($pw, $user['password']);						
			if($isGood){											
				$user['user_type'] = $this->user_type;
				if($this->login($user)){					
					$this->load->model('user/MActiveUser');
					return $this->MActiveUser->set_user($user);
				}				
			}
		}
		return false;	
	}
	function logout(){
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('local_user_id');		
		$this->session->unset_userdata('timezone');
		$this->session->unset_userdata('user_type');
		@session_destroy();		
		session_start();
		session_unset();
		session_destroy();
		session_write_close();
		setcookie(session_name(),'',0,'/');
		session_regenerate_id(true);
	}
	//TODO: update profile not password
	function update_profile($user){
		$now = date("Y-m-d H:i:s");		
		$data = array(			
			'email'=>	$user['email']					
			,'last_update'=>$now			
			);
		$ret= true;
		$this->db->trans_begin();
		$this->db->update(
			$this->m_table,
			 $data,
			 array('id' => $user['id'])
		 );
		
		if ($this->db->affected_rows() == '1')
		{	
			$global_user = $user;		
			$global_user['id'] = $user['userid'];
			$ret= parent::update_profile($global_user);
		}else{			
			$ret = false;
		}
		if ($this->db->trans_status() === FALSE || $ret == false)
		{
			$this->db->trans_rollback();
			$this->add_error('Transaction failed');			
			$ret = false;			
		} 
		else
		{
			$this->db->trans_commit();
			$this->load->model('user/MActiveUser');
			$user['user_type'] = $this->user_type;
			$this->MActiveUser->set_user($user);
		}	
		return $ret;		
	}
	function change_password($username, $oldpass,$newpass){
		$users = $this->get_by(array('username'=>db_clean($username)),0,1);
		
		if($users!=null){
			$user= $users[0];
			$this->load->helper('bcrypt');
			$bcrypt = new Bcrypt(15);
			$isGood = $bcrypt->verify($oldpass, $user['password']);						
			if($isGood){											
				$now = date("Y-m-d H:i:s");
				$bcrypt = new Bcrypt(15);							
				$data = array(			
					'password'=>	$bcrypt->hash($newpass)					
					,'last_update'=>$now			
					);
				$ret= true;				
				$this->db->update(
					$this->m_table,
					 $data,
					 array('username' => $username)
				 );
				
				if ($this->db->affected_rows() == '1')
				{	
										
				}else{			
					$ret = false;
				}	
				return $ret;			
			}
		}
		return false;						
	}
	function getNewPass()
	{
	 	$str = sprintf("%d",mt_rand());
	    $new_password = "";
	    for($i=0;$i<strlen($str);$i++)
	    {
	        $new_password .= chr($str[$i]+65);
	    }
	    srand ((double) microtime() * 1000000);
	    $rand_number = rand(0, 999);
	    $new_password .= $rand_number;
	    return $new_password;		    
	}
	function reset($email)
	{
		$pw = $this->getNewPass();
		$this->load->helper('bcrypt');
		$bcrypt = new Bcrypt(15);		//		
		$now = date("Y-m-d H:i:s");		
		$data = array(			
			'password'=>	$bcrypt->hash($pw)					
			,'last_update'=>$now			
			);
		$ret= true;				
		$this->db->update(
			$this->m_table,
			 $data,
			 array('email' => $email)
		 );
		
		if ($this->db->affected_rows() == '1')
		{	
								
		}else{			
			$ret = false;
		}			
		return $pw;
	}
	function get_by_user_id($userid){
		$users = $this->get_by(array('userid'=>id_clean($userid)),0,1);
		if($users!=null)
			return $users[0];
		return null;
	}
}
?>
