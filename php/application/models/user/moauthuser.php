<?php
require_once dirname(__FILE__).'/muser.php';
class MOAuthUser extends MUser{
	protected $m_table = 'oauth_user';
	protected $user_type=2;
	public $cols = array(
		'id'
		,'oauth_username'
		,'oauth_provider'
		,'oauth_uid'
		,'created_date'		
		,'last_update'			
		,'email'
		,'userid'
		);
	function __construct ()
	{
		parent::__construct();
	}	
	function is_login(){
		if($this->check_twitter_login())
			return TRUE;		
		if($this->check_fb_login())
			return TRUE;
		return FALSE;		
	}	
	function get_user($oauth_provider,$oauth_uid){
		$criteria = array(
			'oauth_provider'=>$oauth_provider
			,'oauth_uid'=>$oauth_uid
		);
		$user = $this->get_by($criteria);
		if($user==null)
			return null;
		return $user[0];
	}
	
	function register($oauth_provider,$oauth_uid,$username,$email){	
		$user = array();
		$now = date("Y-m-d H:i:s");				
		$user['oauth_username'] = $username;
		$user['oauth_uid'] = $oauth_uid;
		$user['oauth_provider'] = $oauth_provider;
		$user['created_date'] = $now;				
		$user['last_update'] = $now;					
		$user['email'] = $email;
		$ret = true;
		$this->db->trans_begin();
		if(parent::register($user)==FALSE){				
			$this->add_error('failed saving global user');
			$ret = false;				
		}
		else
		{						
			$user['userid'] = $user['id'];
			unset($user['id']);					
			if($this->save($user)==FALSE){
				echo('failed saving user');
				$ret = false;
			}			
		}
		
		if ($this->db->trans_status() === FALSE || $ret == false)
		{
			$this->db->trans_rollback();
			echo('Transaction failed');			
			$ret = false;			
		} 
		else
		{
			$this->db->trans_commit();
		}			
			
		if($ret)
			return $user;
		return NULL;
	}
	
	function login($user){
		$user['user_type'] = $this->user_type;
		if(parent::login($user)){					
			$this->load->model('user/MActiveUser');
			return $this->MActiveUser->set_user($user);			
		}
		return FALSE;
	}  
	
	function logout(){
		$this->fb_logout();
	}
	function update_profile($user){
		//do some update
		$global_user = $user;		
		$global_user['id'] = $user['userid'];
		if(parent::update_profile($global_user)){
			$this->load->model('user/MActiveUser');
			$user['user_type'] = $this->user_type;
			$this->MActiveUser->set_user($user);
			return true;	
		}
		return false;
	}	
	/*facebook*/
	function init_facebook(){
		$config = array(
						'appId'  => $this->config->item('fb_app_id'),
						'secret' => $this->config->item('fb_app_secret')
						
						);
		$this->load->library('Facebook', $config);
	}
	function get_facebook_user(){	
		$user = 0;
		for($i=0;$i<100;$i++){		
			$user = $this->facebook->getUser();
			if($user!=0)
				break;			
		}
		//echo "fbid".$user;
		$profile = null;
		$fb_user = null;
		if($user)
		{					
			try {
				$accessToken = $this->facebook->getAccessToken();								
				//$fb_user = $this->facebook->api('/me');								
				$fb_user = $this->facebook->api('/'.$user);					
                //$ses_user=array('fb_user'=>$fb_user);
		        //$this->session->set_userdata($ses_user);
		        //var_dump($fb_user);
            } catch (FacebookApiException $e) {
				var_dump($e);
				exit(0);
                $fb_user = null;
            }
		}
		return $fb_user;
	}
	function check_fb_login(){
		$this->init_facebook();	
		
		$fb_user = $this->get_facebook_user();				
		if ($fb_user) {			            
            $user = $this->get_user('facebook',$fb_user['id']);
            if($user==null){
				$user = $this->register(
					'facebook',$fb_user['id']
					,!empty($fb_user['username'])?$fb_user['username']:$fb_user['name']	
					,$fb_user['email']			
				);
			}
			
			if($user==NULL){
				die('user null');				
			}
			if($this->login($user)){				
				return TRUE;
			}	
        }
        return FALSE;
	}
	
	function fb_logout(){
		$this->init_facebook();	
		setcookie('fbs_'.$this->facebook->getAppId(), '', time()-100, '/', 'fbtweetbot.uni.me');
		//$user = $this->facebook->getUser();		
		//if($user)
		//{
			//$params = array('next' => base_url('login/fblogout'));
			//$url =$this->facebook->getLogoutUrl($params);			

			$this->session_clear();
			//redirect($url);
		//}
	}
	function session_clear(){
		//die('das');
		//$this->init_facebook();	
		//setcookie('fbs_'.$this->facebook->getAppId(), '', time()-100, '/', 'fbtweetbot.uni.me');
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('role');
		$this->session->unset_userdata('local_user_id');		
		$this->session->unset_userdata('timezone');
		$this->session->unset_userdata('user_type');
		session_destroy();		
		session_start();
		session_unset();
		session_destroy();
		session_write_close();
		setcookie(session_name(),'',0,'/');
		session_regenerate_id(true);
		header('Location: /');
	}
	function get_fb_login_url(){
		$this->init_facebook();	
		$params = array(
			'redirect_uri' =>base_url('login/index'),
//			'canvas'=>1,
//			'fbconnect'=>0,
			'scope'=>'read_stream,publish_stream,email'
			);
		return $this->facebook->getLoginUrl($params);
	}
	/*twitter*/
	function init_twitter($oauth_token = NULL, $oauth_token_secret = NULL){
		$config = array(
			'consumer_key'  => $this->config->item('twitter_consumer_key'),
			'consumer_secret' => $this->config->item('twitter_consumer_secret'),	
			'oauth_token'=>$oauth_token,
			'oauth_token_secret'=>$oauth_token_secret
		);
		
		$this->load->library(
			'TwitterOAuth',$config
			);
	}
	function get_twitter_login_url(){
		$this->init_twitter();
		$redirect = base_url('login/index');
		$request_token = $this->twitteroauth->getRequestToken($redirect);
		$this->session->set_userdata(
			'twitter_oauth_token',
			 $request_token['oauth_token']
	 	);
		$this->session->set_userdata(
			'twitter_oauth_token_secret',
			$request_token['oauth_token_secret']
		);		
		if ($this->twitteroauth->http_code == 200) {
		    // Let's generate the URL and redirect
    		$url = $this->twitteroauth->getAuthorizeURL($request_token['oauth_token']);
    		header('Location: ' . $url);
    		exit(0);
		} else {		
    		die('Something wrong happened.');
		}
	}
	function check_twitter_login(){
		$twitter_user = $this->get_twitter_user();				
		if ($twitter_user) {			            
            $user = $this->get_user('twitter',$twitter_user->id);
            if($user==null){
				$user = $this->register(
					'twitter',$twitter_user->id
					,!empty($twitter_user->name)?$twitter_user->name:$twitter_user->screen_name	
					,''			
				);
			}
			
			if($user==NULL){
				die('user null');				
			}
			if($this->login($user)){				
				return TRUE;
			}	
        }
        return FALSE;
		
	}
	function get_twitter_user(){
		$redirect = base_url('login/index');
		$oauth_verifier = $this->input->get('oauth_verifier');
		$twitter_oauth_token =$this->session->userdata('twitter_oauth_token');
		$twitter_oauth_token_secret = $this->session->userdata('twitter_oauth_token_secret'); 
		if (!empty($oauth_verifier)		
			 && !empty($twitter_oauth_token) 
			 && !empty($twitter_oauth_token_secret)
	 	) {
		    $this->init_twitter($twitter_oauth_token,$twitter_oauth_token_secret);
		    $access_token = $this->twitteroauth->getAccessToken(
		    	$this->input->get('oauth_verifier')
	    	);
			// Save it in a session var
		    $this->session->set_userdata('twitter_access_token',$access_token);
			//Let's get the user's info
		    $user_info = $this->twitteroauth->get('account/verify_credentials');		    			
		    if (isset($user_info->error)) {
		        echo $user_info->error;
		        exit(0);
		    } else {
		        //$uid = $user_info->id;
		        //$username = $user_info->name;		        
		        if($user_info!=NULL)
		    		return $user_info;
		    }
		}
	    return NULL;
		
	}
	function get_by_user_id($userid){
		$users = $this->get_by(array('userid'=>id_clean($userid)),0,1);
		if($users!=null)
			return $users[0];
		return null;
	}
}
?>
