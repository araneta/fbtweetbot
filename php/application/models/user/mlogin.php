<?php
//DEPRECATED
//model to check login of a user
class MLogin extends MY_Model
{
	protected $m_table = 'local_user';
	public $cols = array(
		'id'
		,'username'
		,'password'
		,'email'
		,'created_date'
		,'last_login'
		,'last_update'
		,'timezone'
		,'id_language'
		,'role'
		);
	function __construct ()
	{
		parent::__construct();
	}
	function verifyUser($u,$pw)
	{
		$users = $this->get_by(array('username'=>db_clean($u)),0,1);
		if($users!=null){
			$user= $users[0];
			$this->load->helper('bcrypt');
			$bcrypt = new Bcrypt(15);
			$isGood = $bcrypt->verify($pw, $user['password']);
			if($isGood){
				$now = date("Y-m-d H:i:s");	
				$user['last_login'] = $now;			
				$user['last_update'] = $now;
				if($this->save($user)){
					$global_id = $this->get_global_user_id($user['id']);
					if($global_id==NULL)
						return false;
					$this->session->set_userdata('userid',$global_id);					
					$this->session->set_userdata('local_user_id',$user['id']);
					$this->session->set_userdata('username',$user['username']);
					$this->session->set_userdata('role',$user['role']);
					return true;
				}				
			}
		}
		return false;	
	}	
	function get_global_user_id($local_user_id){
		$users = $this->get_data(
			$this->db->get_where('user', array('fk_id' => intval($local_user_id),'user_type'=>1))			
		);
		if($users==NULL)
			return NULL;
		$user = $users[0];
		return $user['id'];
	}
}
