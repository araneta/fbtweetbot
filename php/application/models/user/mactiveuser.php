<?php
require_once dirname(__FILE__).'/muser.php';
//require_once dirname(__FILE__).'/mlocaluser.php';
//require_once dirname(__FILE__).'/moauthuser.php';
class MActiveUser extends MUser{
	protected $user_type;
	function __construct()
	{		
		parent::__construct();
		
	}
	function is_login(){
		if ($this->session->userdata('userid')==FALSE){		
			//check oauth user
			$this->load->model('user/MOauthUser');
			if($this->MOauthUser->is_login()){
				$this->session->set_userdata('user_type',2);						
				return true;
			}			
			return false;
		}		
		$this->session->set_userdata('user_type',1);
		return true;
	}
	function get_user_model(){
		switch ($this->session->userdata('user_type')){
			case 1:						
				$this->load->model('user/MLocalUser');
				return $this->MLocalUser;				
			break;
			case 2:
				$this->load->model('user/MOAuthUser');
				return $this->MOAuthUser;
			break;
		}
		return NULL;
	}
	/*
	 * return active user: localuser or oauthuser
	 * */
	function get_user(){
		$model = $this->get_user_model();
		if($model==NULL)
			return NULL;		

		$user = NULL;
		$user = $model->get($this->get_local_user_id());
		
		if($user==NULL)
			return NULL;
		$user['userid'] = $this->session->userdata('userid');
		$user['user_type'] = $this->session->userdata('user_type');
		$user['role'] = $this->session->userdata('role');
		$user['timezone'] = $this->session->userdata('timezone');	
		return $user;
	}
	function get_local_user_id(){
		return intval($this->session->userdata('local_user_id'));
	}
	/*
	 * set active user
	 * param $locauser or $oauthuser
	 * */
	function set_user($user){
		$global_user = $this->get_global_user($user['userid']);					
		if($global_user==NULL){			
			return false;
		}
		$username = '';
		switch($global_user['user_type']){
			case 1://localuser
				$username = $user['username'];	
			break;
			case 2:
				$username = $user['oauth_username'];
			break;
		}
		$this->session->set_userdata('userid',$global_user['id']);
		$this->session->set_userdata('local_user_id',$user['id']);
		$this->session->set_userdata('username',$username);
		$this->session->set_userdata('role',$global_user['role']);
		$this->session->set_userdata('user_type',$global_user['user_type']);
		$this->session->set_userdata('timezone',$global_user['timezone']);
		return true;	
	}
	function logout(){
		$model = $this->get_user_model();
		if($model!=NULL)
			$model->logout();
		
	}
}
?>