<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//model to register a user into the system
class MUser extends MY_Model
{
	protected $m_table = 'user';
	protected $user_type = 0;
	public $cols = array(
		'id'
		,'user_type'		
		,'last_login'
		,'timezone'
		,'id_language'
		,'role'
		,'created_date'		
		,'last_update'
		,'expired_date'
	);
	
	function __construct()
	{		
		parent::__construct();
		
	}
	function register(&$user){	
		$now = date("Y-m-d H:i:s");		
		$data = array(
			'user_type'=>$this->user_type			
			//,'last_login'=>	$now
			,'id_language'=>1
			,'role'=>'user'
			,'created_date'=>$now
			,'last_update'=>$now
			,'timezone'=>empty($user['timezone'])?'':$user['timezone']	
			);
		
		$this->db->insert('user', $data);
		$user['id'] = $this->db->insert_id();
		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE;
		
		//return $this->save($user);	
		
	}
	
	function get_global_user($userid){
		$user = $this->get(intval($userid));			
		return $user;
	}
	
	function is_login(){
		return false;		
	}	
	function get_user_id(){
		return -1;
	}
	function get_user(){
		return $this;
	}
	
	//save login date
	function login($user){
		$now = date("Y-m-d H:i:s");		
		$data = array(			
			'last_login'=>	$now			
			,'last_update'=>$now			
			);
		$this->db->update(
			'user',
			 $data,
			 array('id' => $user['userid'])
		 );
		
		if ($this->db->affected_rows() == '1')
		{
			return TRUE;		
		}
		
		return FALSE;
	}
	function logout(){
		
	}
	function update_profile($user){
		$now = date("Y-m-d H:i:s");		
		$data = array(			
			'timezone'=>$user['timezone']			
			,'last_update'=>$now			
			);
		$this->db->update(
			'user',
			 $data,
			 array('id' => $user['id'])
		 );
		
		if ($this->db->affected_rows() == '1')
		{
			return TRUE;		
		}
		
		return FALSE;
	}
	function get_expired_date($user_id){
		$user = $this->get($user_id);
		if($user!=null)
			return $user['expired_date'];
		return date("Y-m-d H:i:s");
	}
}

?>
