<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = '';
$route['p/(:any)'] = 'home/p/$1';
/*root*/
$route['admin/accounts/(:any)'] = 'comp/adminautofbtwitter/accounts/$1';
$route['admin/tweetscheduler/(:any)'] = 'comp/adminautofbtwitter/tweetscheduler/$1';
$route['admin/tweetqueue/(:any)'] = 'comp/adminautofbtwitter/tweetqueue/$1';
$route['admin/tweetmessage/(:any)'] = 'comp/adminautofbtwitter/tweetmessage/$1';
$route['admin/fbscheduler/(:any)'] = 'comp/adminautofbtwitter/fbstatusscheduler/$1';
$route['admin/fbfriendscheduler/(:any)'] = 'comp/adminautofbtwitter/fbfriendstatusscheduler/$1';
$route['admin/fbpages/(:any)'] = 'comp/adminautofbtwitter/fbpages/$1';
$route['admin/fbpagescheduler/(:any)'] = 'comp/adminautofbtwitter/fbpagestatusscheduler/$1';
$route['admin/logviewer/(:any)'] = 'comp/adminautofbtwitter/logviewer/$1';
/*/root*/
$route['tweeteditor/(:any)'] = 'comp/autofbtwitter/tweeteditor/$1';
$route['accounts/(:any)'] = 'comp/autofbtwitter/accounts/$1';
$route['tweetscheduler/(:any)'] = 'comp/autofbtwitter/tweetscheduler/$1';
$route['tweetqueue/(:any)'] = 'comp/autofbtwitter/tweetqueue/$1';
$route['tweetmessage/(:any)'] = 'comp/autofbtwitter/tweetmessage/$1';
$route['fbscheduler/(:any)'] = 'comp/autofbtwitter/fbstatusscheduler/$1';
$route['fbfriendscheduler/(:any)'] = 'comp/autofbtwitter/fbfriendstatusscheduler/$1';
$route['fbpages/(:any)'] = 'comp/autofbtwitter/fbpages/$1';
$route['fbpagescheduler/(:any)'] = 'comp/autofbtwitter/fbpagestatusscheduler/$1';
//$route['uploaddir/(:any)'] = 'uploaddir/$i';

//$route['tweetmessage'] = 'comp/autofbtwitter/tweetmessage/index';
/* End of file routes.php */
/* Location: ./application/config/routes.php */
?>
