<?php
class MLog extends MY_Model
{
	protected $m_table = 'com_autofbtwitter_log';
	public $cols = array(
		'id'
		,'descr'		
		,'created_date'
		,'sender'		
		);
	function __construct()
	{		
		parent::__construct();		
	}
	function get_senders(){
		return array(
			'schedule_fb_friend'=>'schedule_fb_friend'
			,'queue_tweet'=>'queue_tweet'
			,'schedule_fb'=>'schedule_fb'
			,'schedule_fbpage'=>'schedule_fbpage'
			,'schedule_tweet'=>'schedule_tweet'
		);
	}
}
