<?php
class MFBPage extends MY_Model
{
	protected $m_table = 'com_autofbtwitter_fbpagetoken';
	public $cols = array(
		'id'
		,'name'
		,'access_token'
		,'category'
		,'page_id'		
		,'created_date'
		,'last_update'
		,'id_user'		
		);
	function __construct()
	{		
		parent::__construct();		
	}
	function save($data){
		$now = date("Y-m-d H:i:s");
		if(empty($data['id'])){						
			$data['created_date'] = $now;			
			$data['last_update'] = $now;
		}else{
			$data['last_update'] = $now;
		}
		return parent::save($data);	
	}
	function save_pages_token($user_id,$content){
		$obj = json_decode($content);
		if($obj!=NULL){
			if($obj->{'data'}!= NULL){
				$this->db->trans_begin();				
				$ret = TRUE;
				$this->db->delete($this->m_table, array('id_user' => id_clean($user_id))); 
				foreach($obj->{'data'} as $page){					
					$data = array(
						'name'=>$page->name
						,'access_token'=>$page->access_token
						,'category'=>$page->category
						,'page_id'=>$page->id
						,'id_user'=>$user_id
					);
					$ret = $ret && $this->save($data);					
				}
				if ($this->db->trans_status() === FALSE || $ret == false)
				{
					$this->db->trans_rollback();
					$this->add_error('Transaction failed');
					$ret = false;			
				} 
				else
				{
					$this->db->trans_commit();
				}				
				return $ret;
			}else{
				return FALSE;
			}			
		}else
		{
			return FALSE;
		}	
	}
}
?>
