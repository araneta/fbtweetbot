<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//account controller
class Accounts extends Comp{
	protected $m_view = 'admin/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
		$this->is('admin');
	}
	/*
	 *controllers
	 */  
	function index(){					
		$this->data['title'] = 'Account List';
		$this->model('MAccount');
		$this->load->helper('table');
					
		//create table		
		$conf['base_url'] = site_url('admin/accounts/index');
		//$conf['per_page'] =  1;		
		$actions[] = array('Delete',site_url('admin/accounts/delete'));		
		$cols[] = array('Username','userusername',200,'scope="col" class="rounded-left"','asc');
		$cols[] = array('Account','username',200,'','asc');
		$cols[] = array('Type','type',100);		
		$cols[] = array('Created Date','created_date',200);				
		
		$empty_msg = '<div class="warning_box">You have not added any account yet. please add</div>';
		$this->data['table'] = create_table($conf,$cols,$actions,
			$this->MAccount,array(),			
			$empty_msg );
		
		$this->view('account_list',$this->data);
	}
		
	function delete(){
		$id = $this->input->post('itemid');
		if(empty($id)){
			$this->session->set_flashdata('error','Item id is empty');
		}
		$id = intval($id);
		$this->model('MAccount');		
		
		$account = $this->MAccount->get($id);
		if($account==null){
			$this->session->set_flashdata('error','Account can not be found');
		}else{			
			$has_dependencies = false;
			
			if($account['type']=='1'){//twitter				
				$this->model('MSchedule');
				if($this->MSchedule->has_schedule($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some tweet schedule. Please delete the schedule first.');				
					$has_dependencies = true;
				}
				$this->model('MQueue');
				if($this->MQueue->has_queue($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some tweet queue. Please delete the tweet queue first.');				
					$has_dependencies = true;
				}
			}else if($account['type']=='2'){//fb								
				$this->model('MFBSchedule');
				if($this->MFBSchedule->has_schedule($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some facebook schedule. Please delete the schedule first.');				
					$has_dependencies = true;
				}
			}
			if(!$has_dependencies){
				if($this->MAccount->delete($id)==TRUE){
					$this->session->set_flashdata('info','Account deleted');
				}else{
					$this->session->set_flashdata('error','Account can not be deleted');
				}
			}
		}
		redirect(base_url('admin/accounts/index') );
	}
	
}
?>
