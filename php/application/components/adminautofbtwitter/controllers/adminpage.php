<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//scheduler controller
class AdminPage extends Comp{
	function get_all_user(){
		$this->load->model('user/MLocalUser');
		$this->load->model('user/MOAuthUser');
		$users = $this->MLocalUser->get_all();
		$data =array();
		$data[0] = 'All';
		if($users!=null){
			foreach($users as $user){				
				$data[$user['userid']] = $user['username'];
			}
		}
		$users = $this->MOAuthUser->get_all();
		if($users!=null){
			foreach($users as $user){				
				$data[$user['userid']] = $user['oauth_username'];
			}
		}
		return $data;
	}
	function selectuser(){
		$id = $this->input->post('username');		
		$id = intval($id);
		$this->load->model('user/MUser');
		$user = $this->MUser->get($id);		
		if($user!=null){

			if($user['user_type']==1){				
				$this->load->model('user/MLocalUser');
				$user = $this->MLocalUser->get_by_user_id($id);				
				$this->session->set_userdata('selusername',$user['username']);
			}else if($user['user_type']==2){
				$this->load->model('user/MOAuthUser');
				$user = $this->MOAuthUser->get_by_user_id($id);
				$this->session->set_userdata('selusername',$user['oauth_username']);				
			}
		}else{
			
		}
		$this->session->set_userdata('seluserid',$id);
		$this->index();
	}
}
?>