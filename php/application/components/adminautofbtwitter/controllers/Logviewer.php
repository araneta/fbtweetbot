<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//scheduler controller
class Logviewer extends Comp{
	protected $m_view = 'admin/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
		$this->is('admin');
		
	}
	function select(){
		$created_date = $this->input->post('created_date');
		if(empty($created_date)){
			$created_date = date("Y-m-d");
		}
		$this->session->set_userdata('created_date',$created_date);
		redirect('admin/logviewer/index');
	}
	function index($offset=0){				
		$created_date = $this->session->userdata('created_date');	
		
		if(!empty($created_date)){
				//convert from mm/dd/yy to yyyy-mm-dd
			list($mm,$dd,$yy)=explode("/",$created_date);
			$created_date = sprintf('%s-%s-%s',$yy,$mm,$dd);
			//convert scheduled time to server time
			$created_date_min  = date("Y-m-d H:i:s", strtotime($created_date . ' 00:00:00'));					
			$created_date_max  = date("Y-m-d H:i:s", strtotime($created_date . ' 23:59:59'));					
		}else{
			$created_date = date("Y-m-d");
			$created_date_min  = date("Y-m-d H:i:s", strtotime($created_date . ' 00:00:00'));					
			$created_date_max  = date("Y-m-d H:i:s", strtotime($created_date . ' 23:59:59'));
		}		
				
		$this->data['title'] = 'Logs';	
		$this->model('MLog');	
		$this->load->helper('table');				
		$conf['base_url'] = site_url('admin/logviewer/index');
		$conf['show_no'] = TRUE;	
		//$conf['per_page'] = 2;
		$actions[] = array('Edit',site_url('admin/logviewer/show'));							
		$cols[] = array('Date','created_date',50,'');		
		$cols[] = array('Sender','sender',50,'');		
		$cols[] = array('Message','descr',100,'');	
		$criteria = array(
			'created_date >='=>$created_date_min,
			'created_date <='=>$created_date_max			
		);
		$this->data['log'] = array('created_date'=>date('m/d/Y',strtotime($created_date)));
		$empty_msg = '<div class="warning_box">Empty</div>';
		$this->data['table'] = create_table($conf,$cols,$actions,$this->MLog,$criteria,$empty_msg );						
		$this->data['senders'] = $this->MLog->get_senders();		
		$this->data['jsfiles'] = array($this->config->item('jqueryui_js'));		
		$this->data['cssfiles'] = array($this->config->item('jqueryui_css'));
		$this->view('log_list',$this->data);
	}
}
