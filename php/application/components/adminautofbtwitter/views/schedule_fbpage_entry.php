<?php
echo form_open(base_url('fbpagescheduler/save'));
echo form_hidden('id',set_value('id',$schedule['id']));	

?>
<fieldset>
	<label>Your message will be scheduled for:</label>
	<dl>	            
		<dt><label for="com_autofbtwitter_fbpagetoken_id">Page Name:</label></dt>
		<dd><?php echo form_dropdown('com_autofbtwitter_fbpagetoken_id', $pages,set_value('com_autofbtwitter_fbpagetoken_id',$schedule['com_autofbtwitter_fbpagetoken_id']),'id="com_autofbtwitter_fbpagetoken_id"'); ?></dd>
	</dl>	
	<dl>
		<dt><label for="schedule_date">Date:</label></dt>
		<dd><?php echo form_input('schedule_date', set_value('schedule_date',$schedule['schedule_date']),'id="schedule_date"'); ?> format: mm/dd/yyyy</dd>
	</dl>
	<dl>
		<dt><label for="schedule_hhmm">Time:</label></dt>
		<dd><?php echo form_input('schedule_hhmm', set_value('schedule_hhmm',$schedule['schedule_hhmm']),'id="schedule_hhmm"'); ?> format: HH:MM (24-hour)</dd>
	</dl>
	<dl>
		<dt><label for="schedule_tz">Time Zone:</label></dt>
		<dd><?php get_tz_options('schedule_tz',set_value('schedule_tz',empty($schedule['id'])?$user['timezone']:$schedule['schedule_tz'])); ?></dd>
	</dl>
	<dl>
		<dt><label for="caption">Caption:</label></dt>
		<dd><?php echo form_input('caption', set_value('caption',$schedule['caption']),'id="caption"'); ?></dd>
	</dl>
	<dl>
		<dt><label for="link">Link:</label></dt>
		<dd><?php echo form_input('link', set_value('link',$schedule['link']),'id="link"'); ?></dd>
	</dl>
	<dl>
		<dt><label for="message">Message:</label></dt>
		<dd><textarea name="message" cols="30" rows="5"  id="message"><?php echo set_value('message',$schedule['message']);?></textarea></dd>
		<div id="imgcont">			
		</div>
	</dl>
	
	<dl>
		<dt>&nbsp;</dt>
		<dd>			
			<input type="button" id="btnImgUpload" value="Add Image" />			
		</dd>		
	</dl>
	<dl>
		<dt><label>&nbsp;</label></dt>
		<dd><?php echo form_submit('submit', 'Save');?><?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('fbpagescheduler/index').'\'"'); ?></dd>
	</dl>
	<?php echo form_hidden('image_file',set_value('image_file',$schedule['image_file'])); ?>
	
</fieldset>
<div id="upload-form"></div>
<?php
echo form_close();
?>
<script type="text/javascript">
$(function() {
	$("#schedule_date").datepicker({
		  showTime: true,
		  constrainInput: false,
		  stepMinutes: 1,
		  stepHours: 1,
		  time24h: false,		  
		  dateFormat: "mm/dd/yy"
		});	
   $("#upload-form" ).dialog({
		autoOpen: false,
		height: 200,
		width: 400,
		modal: true,
		buttons: {
				Ok: function() {				
					$( this ).dialog( "close" );
				}
				,
				Cancel: function() {
					$( this ).dialog( "close" );
				}
		},
		close: function() {                    
		},
		open:function(){                			
			$( "#upload-form" ).load('<?php echo base_url('upload/index');?>',function(){
				$('#upload_file').submit(function(e) {
					$('#files').html('<img src="<?php echo base_url('images/ajax-loader.gif');?>" /><span>Uploading file...</span>'); 
				  e.preventDefault();
				  $.ajaxFileUpload({
					 url         :'<?php echo base_url('fbpagescheduler/upload_file');?>',
					 secureuri      :false,
					 fileElementId  :'userfile',
					 dataType    : 'json',
					 data        : {
						<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
					 },
					 success  : function (data, status)
					 {
						if(data.status != 'error')
						{
						   $('#files').html('');
						   $('input[name="image_file"]').val(data.msg);
						   $('#imgcont').html('<img src="<?php echo base_url(basename($this->config->item('upload_dir')));?>/'+data.msg+'" />');						   
						   $( "#upload-form" ).dialog( "close" );
						}else{
							alert(data.msg);
						}
					 }
				  });
				  return false;
			   });
			});                
		}
	});
	$( "#btnImgUpload" )            
		.click(function() {
			$( "#upload-form" ).dialog( "open" );
	});
	if($('input[name="image_file"]').val()!=''){
		$('#imgcont').html('<img src="<?php echo base_url(basename($this->config->item('upload_dir')));?>/'+$('input[name="image_file"]').val()+'" />');						   
	}
});
</script>
