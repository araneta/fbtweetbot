<?php
if(!empty($table))
	echo '<h2><span class="title">List of Events</span></h2>';
echo form_open(base_url('admin/logviewer/select'));
?>
<fieldset>
	<label>Filter:</label>
	<dl>	            
		<dt><label for="date">Server Date:</label></dt>
		<dd><?php echo form_input('created_date', set_value('created_date',empty($log['created_date'])?'':$log['created_date']),'id="created_date"'); ?> format: mm/dd/yyyy</dd>
	</dl>	
	<dl>
		<dt><label>&nbsp;</label></dt>
		<dd><?php echo form_submit('submit', 'Search');?></dd>
	</dl>
</fieldset>	
<?php
echo form_close();
echo '<div class="clear"></div>';
echo $table;
?>

<script type="text/javascript">
$(function() {	
	$("#created_date").datepicker({
		  showTime: true,
		  constrainInput: false,
		  stepMinutes: 1,
		  stepHours: 1,
		  time24h: false,		  
		  dateFormat: "mm/dd/yy"
		});
});
</script>
