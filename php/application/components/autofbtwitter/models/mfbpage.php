<?php
class MFBPage extends MY_Model
{
	protected $m_table = 'com_autofbtwitter_fbpagetoken';
	public $cols = array(
		'id'
		,'name'
		,'access_token'
		,'category'
		,'page_id'		
		,'created_date'
		,'last_update'
		,'id_user'
		,'com_autofbtwitter_account_id'		
		);
	function __construct()
	{		
		parent::__construct();		
	}
	function save($data){
		$now = date("Y-m-d H:i:s");
		if(empty($data['id'])){						
			$data['created_date'] = $now;			
			$data['last_update'] = $now;
		}else{
			$data['last_update'] = $now;
		}
		return parent::save($data);	
	}
	function save_pages_token($user_id,$account_id,$content){
		$obj = json_decode($content);
		if($obj!=NULL){
			if($obj->{'data'}!= NULL){
				$this->db->trans_begin();				
				$ret = TRUE;
				//$this->db->delete($this->m_table, array('id_user' => id_clean($user_id))); 				
				foreach($obj->{'data'} as $page){					
					//check whether this page is already saved before										
					$existing_data = $this->get_by(
						array('com_autofbtwitter_account_id'=>intval($account_id),'page_id'=>$page->id)
					);					
					//recored exist
					if(count($existing_data) > 0)
					{      						
					   $existing_data = $existing_data[0];					   
					   $data = array(
							'id'=>$existing_data['id']
							,'name'=>$page->name
							,'access_token'=>$page->access_token
							,'category'=>$page->category
							,'page_id'=>$page->id
							,'id_user'=>$user_id
							,'com_autofbtwitter_account_id'=>$account_id
						);
						
					}
					//new
					else{			
											
						$data = array(
							'name'=>$page->name
							,'access_token'=>$page->access_token
							,'category'=>$page->category
							,'page_id'=>$page->id
							,'id_user'=>$user_id
							,'com_autofbtwitter_account_id'=>$account_id
						);
					}
					$ret = $ret && $this->save($data);					
				}
				if ($this->db->trans_status() === FALSE || $ret == false)
				{
					$this->db->trans_rollback();
					$this->add_error('Transaction failed');
					$ret = false;			
				} 
				else
				{
					$this->db->trans_commit();
				}				
				return $ret;
			}else{
				return FALSE;
			}			
		}else
		{
			return FALSE;
		}	
	}
	function delete_by_account($account_id){				                   				
		$query = $this->db->get_where($this->m_table,array('com_autofbtwitter_account_id'=>intval($account_id)));
        if($query->num_rows() > 0)
        {           
           $data = $this->get_data($query);
           foreach($data as $token){			   
				$this->db->delete('com_autofbtwitter_fbpageschedule' ,array('com_autofbtwitter_fbpagetoken_id'=>intval($token['id'])));
		   }
		   $this->db->delete($this->m_table, array('com_autofbtwitter_account_id'=>intval($account_id)));
        }
	}
}
?>
