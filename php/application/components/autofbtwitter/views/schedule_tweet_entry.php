<?php
echo form_open(base_url('tweetscheduler/save'));
echo form_hidden('id',set_value('id',$schedule['id']));	

?>
<fieldset>
	<label>Your message will be scheduled for:</label>
	<dl>	            
		<dt><label for="username">Username:</label></dt>
		<dd><?php echo form_dropdown('username', $usernames,set_value('username',$schedule['com_autofbtwitter_account_id']),'id="username"'); ?></dd>
	</dl>	
	<dl>
		<dt><label for="schedule_date">Date:</label></dt>
		<dd><?php echo form_input('schedule_date', set_value('schedule_date',$schedule['schedule_date']),'id="schedule_date"'); ?> format: mm/dd/yyyy</dd>
	</dl>
	<dl>
		<dt><label for="schedule_hhmm">Time:</label></dt>
		<dd><?php echo form_input('schedule_hhmm', set_value('schedule_hhmm',$schedule['schedule_hhmm']),'id="schedule_hhmm"'); ?> format: HH:MM (24-hour)</dd>
	</dl>
	<dl>
		<dt><label for="schedule_tz">Time Zone:</label></dt>
		<dd><?php get_tz_options('schedule_tz',set_value('schedule_tz',empty($schedule['id'])?$user['timezone']:$schedule['schedule_tz'])); ?></dd>
	</dl>
	<dl>
		<dt><label for="message">Message:</label></dt>
		<dd>
			<span>Length: </span><span id="msglength"></span><br /><textarea name="message" cols="30" rows="5" id="message"><?php echo set_value('message',$schedule['message']);?></textarea>
			<br />
			<input type="button" id="btnShortLink" value="Tiny Links" />
		</dd>
	</dl>
	<dl>
		<dt><label for="link">Share:</label></dt>
		<dd>
		<div id="tabs" style="width:400px">
		    <ul>
		        <li><a href="#tabs-1">Image</a></li>		        		        
		    </ul>
		    <div id="tabs-1">		        
		        <div id="imgarea">
		        	<div id="imgcont"></div>
		        	<input type="button" id="btnImgUpload" value="Add Image" />
		        	<input onclick="removeImg();return false;" type="button" id="btnImgRemove" value="Remove Image" />
		        </div>	
		    </div>		        
		</div>
		</dd>
	</dl>			
	<dl>
		<dt><label>&nbsp;</label></dt>
		<dd><?php echo form_submit('submit', 'Save');?><?php echo form_button('cancel', 'Cancel', 'onClick="window.location=\''.base_url('tweetscheduler/index').'\'"'); ?></dd>
	</dl>
	<?php echo form_hidden('image_file',set_value('image_file',$schedule['image_file'])); ?>
</fieldset>
<div id="upload-form" title="upload"></div>
<?php
echo form_close();
?>
<script type="text/javascript">
$(function() {
	$("#tabs").tabs(
		{			
			selected: 1
		}
	);
	$("#schedule_date").datepicker({
		  showTime: true,
		  constrainInput: false,
		  stepMinutes: 1,
		  stepHours: 1,
		  time24h: false,		  
		  dateFormat: "mm/dd/yy"
		});		
   
   
		
	$( "#upload-form" ).dialog({
		autoOpen: false,
		height: 200,
		width: 400,
		modal: true,
		buttons: {
				Ok: function() {				
						$( this ).dialog( "close" );
				}
				,
				Cancel: function() {
					$( this ).dialog( "close" );
				}
		},
		close: function() {                    
		},
		open:function(){                			
			$( "#upload-form" ).load('<?php echo base_url('upload/index');?>',function(){
				$('#upload_file').submit(function(e) {
					$('#files').html('<img src="<?php echo base_url('images/ajax-loader.gif');?>" /><span>Uploading file...</span>'); 
				  e.preventDefault();
				  $.ajaxFileUpload({
					 url         :'<?php echo base_url('tweetscheduler/upload_file');?>',
					 secureuri      :false,
					 fileElementId  :'userfile',
					 dataType    : 'json',
					 data        : {
						<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
						'params':$('#username').val()
					 },
					 success  : function (data, status)
					 {
						if(data.status != 'error')
						{
						   $('#files').html('');						   
						   $('input[name="image_file"]').val(data.msg);			
						   $('#imgcont').html('<img width="350" height="220" src="<?php echo base_url(basename($this->config->item('upload_dir')));?>/'+data.msg+'" />');						   			   
						   $( "#upload-form" ).dialog( "close" );
						}else{
							alert(data.msg);
						}
					 }
				  });
				  return false;
			   });
			});                
		}
	});
	$( "#btnImgUpload" )            
		.click(function() {
				$( "#upload-form" ).dialog( "open" );
	});
	$('#btnShortLink').click(function(){		
		$.post(
			'<?php echo base_url('tweetscheduler/shorten_links');?>',
			{
				text:$('#message').val(),
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'
			},
			function(data){
				if(data!=null)
				{
					$('#message').val(jQuery.trim(data));
				}
			}
		);
	});
	setInterval(function(){
		$('#msglength').html($('#message').val().length+'/140');
	},1000);
	if($('input[name="image_file"]').val()!=''){
		$('#imgcont').html('<img width="350" height="220" src="<?php echo base_url(basename($this->config->item('upload_dir')));?>/'+$('input[name="image_file"]').val()+'" />');						   
	}
});
function removeImg(){
	$('#previmg').remove();
	$('#imgcont').html('');
	$('input[name="image_file"]').val('');
}
</script>
