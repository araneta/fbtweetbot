<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//account controller
class Accounts extends Comp{
	protected $m_view = 'user/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
	}
	/*
	 *controllers
	 */  
	function index(){					
		$this->data['title'] = 'Account List';
		$this->model('MAccount');
		$this->load->helper('table');
					
		//create table		
		$conf['base_url'] = site_url('accounts/index');
		//$conf['per_page'] =  1;		
		$actions[] = array('Delete',site_url('accounts/delete'));		
		$cols[] = array('Username','username',200,'scope="col" class="rounded-left"','asc');
		$cols[] = array('Type','type',100);		
		
		$empty_msg = '<div class="warning_box">You have not added any account yet. please add</div>';
		$this->data['table'] = create_table($conf,$cols,$actions,
			$this->MAccount,array('id_user'=>$this->session->userdata('userid')),			
			$empty_msg );
		
		$this->view('account_list',$this->data);
	}
	//deprecated
	/*
	function addtwitter(){
		redirect($this->get_twitter_auth_url());		
	}
	*/
/*twitter*/
	function init_twitter($oauth_token = NULL, $oauth_token_secret = NULL){
		$config = array(
			'consumer_key'  => $this->config->item('twitter_consumer_key'),
			'consumer_secret' => $this->config->item('twitter_consumer_secret'),	
			'oauth_token'=>$oauth_token,
			'oauth_token_secret'=>$oauth_token_secret
		);
		
		$this->load->library(
			'TwitterOAuth',$config
			);
	}
	function get_twitter_login_url(){
		$this->init_twitter();
		$redirect = base_url('accounts/twittercallback');
		$request_token = $this->twitteroauth->getRequestToken($redirect);
		$this->session->set_userdata(
			'twitter_oauth_token',
			 $request_token['oauth_token']
	 	);
		$this->session->set_userdata(
			'twitter_oauth_token_secret',
			$request_token['oauth_token_secret']
		);		
		if ($this->twitteroauth->http_code == 200) {
		    // Let's generate the URL and redirect
    		$url = $this->twitteroauth->getAuthorizeURL($request_token['oauth_token']);
    		header('Location: ' . $url);
    		exit(0);
		} else {		
    		die('Something wrong happened.');
		}
	}
	function addtwitter(){		
		$this->get_twitter_login_url();
	}
	function addfacebook(){
		//get code from fb
		redirect($this->get_facebook_auth_url());
		
	}
	function twittercallback(){
		$redirect = base_url('accounts/twittercallback');
		$oauth_verifier = $this->input->get('oauth_verifier');
		$twitter_oauth_token =$this->session->userdata('twitter_oauth_token');
		$twitter_oauth_token_secret = $this->session->userdata('twitter_oauth_token_secret'); 
		if (!empty($oauth_verifier)		
			 && !empty($twitter_oauth_token) 
			 && !empty($twitter_oauth_token_secret)
	 	) {
		    $this->init_twitter($twitter_oauth_token,$twitter_oauth_token_secret);
		    $access_token = $this->twitteroauth->getAccessToken(
		    	$this->input->get('oauth_verifier')
	    	);
			// Save it in a session var
		    $this->session->set_userdata('twitter_access_token',$access_token);
			//Let's get the user's info
		    $user_info = $this->twitteroauth->get('account/verify_credentials');		    			
		    if (isset($user_info->error)) {		        
		        $this->session->set_flashdata('info',$user_info->error);		        
		    } else {
		        //$uid = $user_info->id;
		        //$username = $user_info->name;		        
		        if($user_info!=NULL){
		        	$this->model('MAccount');
		        	if($this->is_twitter_user_exists($user_info->name)){
						$this->session->set_flashdata('error','This twitter account already registered, please use another account');
					}else{
			        	if(!$this->save_twitter
			        		(
				        		$this->session->userdata('userid'),
								$user_info->name,
								$access_token['oauth_token'],
								$access_token['oauth_token_secret']
							)
						)
						{
							$this->session->set_flashdata('error',$this->MAccount->print_error());
						}else
						{
							$this->session->set_flashdata('info','Account Saved');
						}
					}
		        }
		    }
		}else{
			$this->session->set_flashdata('error','twitter token is empty');
		}
	    redirect(base_url('accounts/index'),'refresh' );
	}
	//deprecated
	/*
	function twittercallback(){
		if(isset($_GET['oauth_token'])){	
			include 'lib/EpiCurl.php';
			include 'lib/EpiOAuth.php';
			include 'lib/EpiTwitter.php';
			include 'lib/secret.php';
			
			$twitterObj = new EpiTwitter($consumer_key, $consumer_secret);
			$twitterObj->setToken($_GET['oauth_token']);
			$token = $twitterObj->getAccessToken();
			$twitterObj->setToken($token->oauth_token, $token->oauth_token_secret);	  				
			$twitterInfo= $twitterObj->get_accountVerify_credentials();
			echo $twitterInfo->response;
			if(empty($token->oauth_token)||empty($token->oauth_token_secret)){
				$this->session->set_flashdata('error','twitter token is empty');	
			}else
			{				
				//TODO: check twitterinfo		
				var_dump($twitterInfo);exit(0);
				$username = $twitterInfo->screen_name;	
				if(empty($username)){
					$this->session->set_flashdata('error','twitter username is empty');	
				}else{
					if(!$this->save_twitter($this->session->userdata('userid'),
					$username,$token->oauth_token,$token->oauth_token_secret))
					{
						$this->session->set_flashdata('error',$this->MAccount->print_error());
					}else
					{
						$this->session->set_flashdata('info','Account Saved');
					}
				}						
			}
		}else{
			$this->session->set_flashdata('error','twitter token is empty');
		}
		redirect(base_url('accounts/index'),'refresh' );
	}
	*/
	
	function fbcallback(){
		if(isset($_GET['code'])){
			//get access token						
			$content = $this->get_content($this->get_facebook_token_url($_GET['code']));
			if(!empty($content)){
				//echo $content;
				parse_str($content);
				//echo "token:".$access_token;
				if(isset($access_token)){
					$username = $this->get_facebook_username($access_token);
					if($username==null){
						$this->session->set_flashdata('error','Facebook username is empty');	
					}else{
						$this->model('MAccount');
						if($this->is_facebook_user_exists($username)){
							$this->session->set_flashdata('error','This facebook account already registered, please use another account');
						}else{
							if(!$this->save_facebook($this->session->userdata('userid'),$username,$access_token)){						
								$this->session->set_flashdata('error',$this->MAccount->print_error());
							}else{
								$this->session->set_flashdata('info','Account Saved');
							}
						}
					}
				}else
				{
					$this->session->set_flashdata('error','Facebook token is empty:parse error');	
				}
			}else
			{
				$this->session->set_flashdata('error','Facebook token is empty');
			}
		}else{
			$this->session->set_flashdata('error','Facebook code is empty');
		}
		redirect(base_url('accounts/index') );
	}
	function delete(){
		$id = $this->input->post('itemid');
		if(empty($id)){
			$this->session->set_flashdata('error','Item id is empty');
		}
		$id = intval($id);
		$this->model('MAccount');
		$criteria = array('id'=>$id,'id_user'=>intval($this->session->userdata('userid')));
		
		$accounts = $this->MAccount->get_by($criteria);
		if($accounts==null){
			$this->session->set_flashdata('error','Account can not be found');
		}else{
			$account = $accounts[0];
			//$has_dependencies = false;
			
			if($account['type']=='Twitter'){//twitter	
				/*			
				$this->model('MSchedule');
				if($this->MSchedule->has_schedule($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some tweet schedule. Please delete the schedule first.');				
					$has_dependencies = true;
				}
				$this->model('MQueue');
				if($this->MQueue->has_queue($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some tweet queue. Please delete the tweet queue first.');				
					$has_dependencies = true;
				}*/
				$this->model('MQueue');
				$this->MQueue->delete_by_account($id);
				$this->model('MSchedule');
				$this->MSchedule->delete_by_account($id);
			}else if($account['type']=='Facebook'){//fb								
				/*
				$this->model('MFBSchedule');
				if($this->MFBSchedule->has_schedule($id)){
					$this->session->set_flashdata('error','Account can not be deleted because it has some facebook schedule. Please delete the schedule first.');				
					$has_dependencies = true;
				}*/
				$this->model('MFBSchedule');
				$this->MFBSchedule->delete_by_account($id);
				$this->model('MFBPage');
				$this->MFBPage->delete_by_account($id);
				$this->model('MFBFriendSchedule');
				$this->MFBFriendSchedule->delete_by_account($id);
			}
			
			//if(!$has_dependencies){
				if($this->MAccount->delete($id)==TRUE){
					$this->session->set_flashdata('info','Account deleted');
				}else{
					$this->session->set_flashdata('error','Account can not be deleted');
				}
			//}
		}
		redirect(base_url('accounts/index') );
	}
	/*
	 * process	 
	*/
	function is_twitter_user_exists($username){		
		$account = array('type'=>1				
				,'username'=>$username
				);
		$users = $this->MAccount->get_by($account);
		if($users==NULL)
			return FALSE;
		return TRUE;				
	}
	function is_facebook_user_exists($username){		
		$account = array('type'=>2				
				,'username'=>$username
				);
		$users = $this->MAccount->get_by($account);
		if($users==NULL)
			return FALSE;
		return TRUE;				
	}
	function save_facebook($userid,$username,$access_token){		
		$account = array('type'=>2
				,'id_user'=>$userid
				,'username'=>$username
				);
		$accountex = array('access_token'=>$access_token	
				);
		return $this->MAccount->save($account,$accountex);
		
	}
	function save_twitter($userid,$username,$auth_token,$auth_token_secret){		
		$account = array('type'=>1
				,'id_user'=>$userid
				,'username'=>$username
				);
		$accountex = array('token'=>$auth_token
				,'token_secret'=>$auth_token_secret	
				);
		return $this->MAccount->save($account,$accountex);
		
	}
	
	function get_content($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	//deprecated
	/*
	function get_twitter_auth_url(){
		include 'lib/EpiCurl.php';
		include 'lib/EpiOAuth.php';
		include 'lib/EpiTwitter.php';
		include 'lib/secret.php';		
		$twitterObj = new EpiTwitter($consumer_key, $consumer_secret);	
		return $twitterObj->getAuthorizationUrl();
	}
	*/
	//get code
	function get_facebook_auth_url(){
		//include 'lib/secret.php';		
		$url = sprintf('https://www.facebook.com/dialog/oauth?client_id=%s&scope=publish_stream,offline_access&redirect_uri=%s',
			$this->config->item('fb_app_id'),urlencode(base_url().'accounts/fbcallback'));
		return $url;		
	}
	//get token
	function get_facebook_token_url($code){
		//include 'lib/secret.php';	
		$code = str_replace('#_=_','',$code);
		$url = sprintf('https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&code=%s&redirect_uri=%s',
			$this->config->item('fb_app_id'),$this->config->item('fb_app_secret'),$code,urlencode(base_url().'accounts/fbcallback'));
		return $url;
		
	}
	function get_facebook_username($token){
		$url = sprintf('https://graph.facebook.com/me?access_token=%s',$token);
		$content = $this->get_content($url);
		if(!empty($content)){
			$obj = json_decode($content);
			return $obj->{'username'}; 
		}
		return null;
	} 
}

?>
