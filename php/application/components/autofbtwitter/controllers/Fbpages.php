<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//scheduler controller
class Fbpages extends Comp{
	protected $m_view = 'user/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
	}
	/*
	 *controllers
	 */  
	 
	function index($offset=0){					
		$this->data['title'] = 'Facebook Pages List';
		$this->load->helper('table');	
		$this->model('MFBPage');					
		$conf['base_url'] = site_url('fbpages/index');		
		$actions[] = array('Delete',site_url('fbpages/delete'));
		$cols[] = array('Name','name',200,'scope="col" class="rounded-left"','asc');
		$cols[] = array('Category ','category',50,'','asc');						
		$criteria = array('com_autofbtwitter_fbpagetoken.id_user'=>$this->session->userdata('userid'));
		$empty_msg = '<div class="warning_box">You have not added any facebook page yet. please add</div>';
		$this->data['table'] = create_table($conf,$cols,$actions,$this->MFBPage,$criteria,$empty_msg );
		
		$this->view('fbpage_list',$this->data);
	}
	function add(){
		redirect($this->get_facebook_auth_url());
	}
	function delete(){
		$id = $this->input->post('itemid');
		if(empty($id)){
			$this->session->set_flashdata('error','Item id is empty');
		}
		$id = intval($id);
		$this->model('MFBPage');
		$criteria = array('id'=>$id,'id_user'=>intval($this->session->userdata('userid')));
		
		$page = $this->MFBPage->get_by($criteria);
		if($page==null){
			$this->session->set_flashdata('error','page can not be found');
		}else{			
			$has_dependencies = false;
			$this->model('MFBPageSchedule');
			if($this->MFBPageSchedule->has_schedule($id)){
				$this->session->set_flashdata('error','Page can not be deleted because it has some schedule. Please delete the facebook page schedule first.');				
				$has_dependencies = true;
			}
			if(!$has_dependencies){
				if($this->MFBPage->delete($id)==TRUE){
					$this->session->set_flashdata('info','Page deleted');
				}else{
					$this->session->set_flashdata('error','Page can not be deleted');
				}
			}			
		}
		redirect(base_url('fbpages/index') );
	}
	function fbcallback(){		
		if(isset($_GET['code'])){					
			$content = $this->get_content($this->get_facebook_token_url($_GET['code']));
			if(!empty($content)){
				parse_str($content);
				//echo "token:".$access_token;
				if(isset($access_token)){
					$content2 = $this->get_content($this->get_facebook_page_token_url($access_token));
					if(!empty($content2)){
						//echo $access_token;
						$username = $this->get_facebook_username($access_token);
						//echo $username;						
						$this->model('MAccount');
						//echo 'userid'.$this->session->userdata('userid');						
						$account_id = $this->MAccount->get_account_id(intval($this->session->userdata('userid')),$username);
						//echo 'account id'.$account_id;
						if($account_id==NULL){
							$this->session->set_flashdata('error','Please add this facebook account:'.$username.' to Account List first');
						}else
						{
							if($this->save_pages_token($account_id ,$content2)==FALSE){
								$this->session->set_flashdata('error','Error Saving Pages Token');
							}else
							{
								$this->session->set_flashdata('info','facebook pages token Saved');
							}
						}
					}else{
					}					
				}else
				{
					$this->session->set_flashdata('error','Facebook access token is empty:parse error');
				}				
			}else
			{
				$this->session->set_flashdata('error','Facebook access token is empty');
			}
		}else{
			$this->session->set_flashdata('error','Facebook code is empty');
		}
		redirect(base_url('fbpages/index') );
	}
	/*process*/
	function get_facebook_auth_url(){
		//include 'lib/secret.php';
		$url = sprintf("https://www.facebook.com/dialog/oauth?client_id=%s&client_secret=%s&redirect_uri=%s&scope=publish_stream,offline_access,read_stream,manage_pages",
			$this->config->item('fb_app_id'),$this->config->item('fb_app_secret'),urlencode(base_url().'fbpages/fbcallback'));
		return $url;
	}
	function get_facebook_token_url($code){
		//include 'lib/secret.php';
		//$url = sprintf("https://graph.facebook.com/me/accounts?access_token=%s",$access_token);
		$url = sprintf("https://graph.facebook.com/oauth/access_token?client_id=%s&client_secret=%s&redirect_uri=%s&code=%s",
			$this->config->item('fb_app_id'),$this->config->item('fb_app_secret'),urlencode(base_url().'fbpages/fbcallback'),$code);
		
		return $url;
	}
	function get_facebook_page_token_url($access_token){
		//include 'lib/secret.php';
		$url = sprintf("https://graph.facebook.com/me/accounts?access_token=%s",$access_token);
		return $url;
	}
	function get_content($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}
	function save_pages_token($account_id ,$content){		
		$this->model('MFBPage');
		return $this->MFBPage->save_pages_token(
			$this->session->userdata('userid'),
			$account_id,
			$content
		);			
	}
	function get_facebook_username($token){
		$url = sprintf('https://graph.facebook.com/me?access_token=%s',$token);
		$content = $this->get_content($url);
		if(!empty($content)){
			$obj = json_decode($content);
			return $obj->{'username'}; 
		}
		return null;
	} 
}
