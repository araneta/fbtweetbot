<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//scheduler controller
class Fbpagestatusscheduler extends Comp{
	protected $m_view = 'user/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
	}
	/*
	 *controllers
	 */  
	 
	function index($offset=0){					
		$this->data['title'] = 'Schedule for My Pages';	
		$this->model('MFBPage');
		//check whether user has fb pages		
		$criteria = array('id_user'=>intval($this->session->userdata('userid')));
		$pages = $this->MFBPage->get_by($criteria);
		
		if($pages==null)
		{			
			$this->data['hasaccount'] = false;
		}else
		{
			$this->data['hasaccount'] = true;
			$this->model('MFBPageSchedule');
			$this->load->helper('table');					
			$conf['base_url'] = site_url('fbpagescheduler/index');
			$actions[] = array('Delete',site_url('fbpagescheduler/delete'));		
			$actions[] = array('Edit',site_url('fbpagescheduler/edit'));	
			$cols[] = array('Page Name','name',200,'scope="col" class="rounded-left"','asc');
			$cols[] = array('Date ','schedule_date',50,'','asc');				
			$cols[] = array('Time','schedule_hhmm',50,'');				
			$cols[] = array('Timezone','schedule_tz',100,'');		
			$cols[] = array('Message','message',100,'');		
			$cols[] = array('Status','status',100,'');		
			$criteria = array('com_autofbtwitter_fbpageschedule.id_user'=>$this->session->userdata('userid'));
			$empty_msg = '<div class="warning_box">You have not added any schedule yet. please add</div>';
			$this->data['table'] = create_table($conf,$cols,$actions,$this->MFBPageSchedule,$criteria,$empty_msg );
		}			
		$this->view('schedule_fbpage_list',$this->data);
	}
	 
	function add(){		
		$this->load->helper('timezone');
		$this->load->model('user/MActiveUser');
		
		$this->data['title'] = 'Add Schedule for My Pages';
		$this->data['jsfiles'] = array($this->config->item('jqueryui_js'),'ajaxfileupload.js');
		$this->data['cssfiles'] = array($this->config->item('jqueryui_css'));					
		$this->data['pages'] = $this->get_fb_pages();
		$this->data['schedule'] = null;
		$this->data['user'] = $this->MActiveUser->get_user();
		$this->view('schedule_fbpage_entry',$this->data);
	}
	function edit(){
		$id = $this->input->post('itemid');
		if(empty($id))
			die('invalid itemid');
		$id = intval($id);
		$this->model('MFBPageSchedule');
		$criteria = array('com_autofbtwitter_fbpageschedule.id'=>$id,'com_autofbtwitter_fbpageschedule.id_user'=>intval($this->session->userdata('userid')));
		$schedule = $this->MFBPageSchedule->get_by($criteria);
		if($schedule==null)
		{
			$this->session->set_flashdata('error','schedule can not be found');
			redirect(base_url('fbpagescheduler/index') );
			return;
		}
		$this->load->helper('timezone');
		
		$this->data['title'] = 'Edit Schedule for My Pages';
		$this->data['jsfiles'] = array($this->config->item('jqueryui_js'),'ajaxfileupload.js');
		$this->data['cssfiles'] = array($this->config->item('jqueryui_css'));		
		$this->data['pages'] =$this->get_fb_pages();
		$this->data['schedule'] = $schedule[0];				
		$this->view('schedule_fbpage_entry',$this->data);
	}
	
	function delete(){
		$id = $this->input->post('itemid');
		if(empty($id)){
			$this->session->set_flashdata('error','Item id is empty');
		}
		$id = intval($id);
		$this->model('MFBPageSchedule');
		$criteria = array('com_autofbtwitter_fbpageschedule.id'=>$id,'com_autofbtwitter_fbpageschedule.id_user'=>intval($this->session->userdata('userid')));
		
		$schedule = $this->MFBPageSchedule->get_by($criteria);
		if($schedule==null){
			$this->session->set_flashdata('error','Schedule can not be found');
		}else{
			if($this->MFBPageSchedule->delete($id)==TRUE){
				$this->session->set_flashdata('info','Schedule deleted');
			}else{
				$this->session->set_flashdata('error','Schedule can not be deleted');
			}
		}
		redirect(base_url('fbpagescheduler/index') );
	}
	
	function upload_file(){		
		$file_element_name = 'userfile';
		$status = "";
		$msg = "";
			   
		$config['upload_path'] = $this->config->item('upload_dir');
		$config['allowed_types'] =  $this->config->item('upload_file_type');
		$config['max_size']  = $this->config->item('max_size');
		$config['encrypt_name'] = TRUE;
 
		$this->load->library('upload', $config);
 
		if (!$this->upload->do_upload($file_element_name))
		{
			$status = 'error';
			$msg = $this->upload->display_errors('', '');
		}
		else
		{
			$data = $this->upload->data();			
			$image = $data['full_path'];
			$status = "success";
			//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
			$msg = $data['file_name'];
			//@unlink($image);					

		}
		@unlink($_FILES[$file_element_name]);
	   
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}	

	
	/*process*/
	function get_fb_pages(){
		$this->model('MFBPage');
		$criteria = array('id_user'=>intval($this->session->userdata('userid')));
		$pages = $this->MFBPage->get_by($criteria);
		$data =array();
		if($pages!=null){
			foreach($pages as $page){
				$data[$page['id']] = $page['name'];
			}
		}
		return $data;
	}
	function page_check($str)
	{
		$this->model('MFBPage');
		$page = $this->MFBPage->get(intval($str));
		
		if($page !=null){
			if(intval($page['id_user'])==intval($this->session->userdata('userid'))){
				return TRUE;
			}
		}
		$this->form_validation->set_message('page_check','Invalid Page');
		return FALSE;
		
	}
	function checkDate($date)
	{
		if (!isset($date) || $date=="")
		{
			return FALSE;
		}
	   
		list($mm,$dd,$yy)=explode("/",$date);
		if ($dd!="" && $mm!="" && $yy!="")
		{
			return checkdate($mm,$dd,$yy);
		}
	   
		return FALSE;
	}
	function schedule_date_check($str){
		if($this->checkDate($str)==FALSE){			
			$this->form_validation->set_message('schedule_date_check','Invalid Date');
			return FALSE;
		}
		return TRUE;
		
	}
	function date_check($str){
		if($this->checkDate($str)==FALSE){			
			$this->form_validation->set_message('date_check','Invalid Date');
			return FALSE;
		}
		return TRUE;
		
	}
	function hhmm_check($str){
		list($hh,$mm)=explode(":",$str);
		if($hh!='' && $mm !=''){
			if(intval($hh)>=0 && intval($hh)<24 && intval($mm)>=0 && intval($mm)<60){
				return TRUE;
			}			
		}
		$this->form_validation->set_message('hhmm_check','Invalid Time');
		return FALSE; 
	}
	function timezone_check($str){		
		try{
			new DateTimeZone($str);
		}catch(Exception $e){
			$this->form_validation->set_message('timezone_check','Invalid Timezone');
			return FALSE;
		}
		return TRUE;

	}
	function url_check($str){
		$ret = TRUE;
		if(empty($str)){
			
		}
		else{
			//$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
			$pattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
			if (!preg_match($pattern, $str))
			{
				$ret = FALSE;
			}
		}
		if(!$ret)
			$this->form_validation->set_message('url_check','Invalid URL');
        return $ret;
	}
	function saveValidate()
	{		
		$this->form_validation->set_rules('com_autofbtwitter_fbpagetoken_id', 'lang:Page', 'required|integer|callback_page_check');
		$this->form_validation->set_rules('schedule_date', 'lang:Schedule date', 'required|callback_date_check');
		$this->form_validation->set_rules('schedule_hhmm','lang:Schedule time', 'required|callback_hhmm_check');
		$this->form_validation->set_rules('schedule_tz', 'lang:Time zone', 'required|callback_timezone_check');
		$this->form_validation->set_rules('message', 'lang:Message', 'required');
		$this->form_validation->set_rules('link', 'lang:Link', 'trim|prep_url|callback_url_check|xss_clean');
		$this->form_validation->set_rules('caption', 'lang:Caption', 'trim|xss_clean');
		$this->form_validation->set_rules('image_file', 'lang:Image', 'trim|xss_clean');
		$this->form_validation->set_rules('id', 'lang:Id', 'trim|xss_clean|integer');
		
		return ($this->form_validation->run());
	}
	function save(){
		if($this->saveValidate()==FALSE)
		{
			$this->add();
			return;
		}
		else         
		{			
			$this->model('MFBPageSchedule');			
			$schedule = $this->bind($this->MFBPageSchedule);
			$schedule['com_autofbtwitter_account_id'] = intval($this->input->post('username'));			
			$schedule['id_user'] = intval($this->session->userdata('userid'));
			if($this->MFBPageSchedule->save($schedule)==TRUE)				
				$this->session->set_flashdata('info','Schedule saved');
			else
				$this->session->set_flashdata('error','Error saving schedule');			
		}		
		redirect(base_url('fbpagescheduler/index'));
	}
	

}
?>
