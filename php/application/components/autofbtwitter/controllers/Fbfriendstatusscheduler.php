<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//scheduler controller
class Fbfriendstatusscheduler extends Comp{
	protected $m_view = 'user/dashboard';
	public function __construct() {		
		parent::__construct();
		//FAIL	
		$this->need_login();
	}
	/*
	 *controllers
	 */  
	 
	function index($offset=0){					
		$this->data['title'] = 'Schedule for My Friends';					
		$this->model('MFBFriendSchedule');	
		$this->load->helper('table');				
		$conf['base_url'] = site_url('fbfriendscheduler/index');
		$actions[] = array('Delete',site_url('fbfriendscheduler/delete'));		
		$actions[] = array('Edit',site_url('fbfriendscheduler/edit'));	
		$cols[] = array('Sender','sender',100,'scope="col" class="rounded-left"','asc');
		$cols[] = array('To','username',100,'','asc');
		$cols[] = array('Date ','schedule_date',50,'','asc');				
		$cols[] = array('Time','schedule_hhmm',50,'');				
		$cols[] = array('Timezone','schedule_tz',100,'');		
		$cols[] = array('Message','message',100,'');		
		$cols[] = array('Status','status',100,'');		
		$criteria = array('com_autofbtwitter_fbfriendschedule.id_user'=>$this->session->userdata('userid'));
		$empty_msg = '<div class="warning_box">You have not added any schedule yet. please add</div>';
		$this->data['table'] = create_table($conf,$cols,$actions,$this->MFBFriendSchedule,$criteria,$empty_msg );
		
		$this->view('schedule_fb_friend_list',$this->data);
	}
	 
	function add(){		
		$this->load->helper('timezone');
		$this->load->model('user/MActiveUser');
		
		$this->data['title'] = 'Add Schedule for My Friend';
		$this->data['jsfiles'] = array($this->config->item('jqueryui_js'),'ajaxfileupload.js');
		$this->data['cssfiles'] = array($this->config->item('jqueryui_css'));							
		$this->data['schedule'] = null;
		$this->data['user'] = $this->MActiveUser->get_user();
		$this->data['usernames'] =$this->get_fb_users();
		$this->view('schedule_fb_friend_entry',$this->data);
	}
	function edit(){
		$id = $this->input->post('itemid');
		if(empty($id))
			die('invalid itemid');
		$id = intval($id);
		$this->model('MFBFriendSchedule');
		$criteria = array('com_autofbtwitter_fbfriendschedule.id'=>$id,'com_autofbtwitter_fbfriendschedule.id_user'=>intval($this->session->userdata('userid')));
		$schedule = $this->MFBFriendSchedule->get_by($criteria);
		if($schedule==null)
		{
			$this->session->set_flashdata('error','schedule can not be found');
			redirect(base_url('fbfriendscheduler/index') );
			return;
		}
		$this->load->helper('timezone');
		
		$this->data['title'] = 'Edit Schedule for My Friend';
		$this->data['jsfiles'] = array($this->config->item('jqueryui_js'),'ajaxfileupload.js');
		$this->data['cssfiles'] = array($this->config->item('jqueryui_css'));
		$this->data['usernames'] =$this->get_fb_users();				
		$this->data['schedule'] = $schedule[0];				
		$this->view('schedule_fb_friend_entry',$this->data);
	}
	
	function delete(){
		$id = $this->input->post('itemid');
		if(empty($id)){
			$this->session->set_flashdata('error','Item id is empty');
		}
		$id = intval($id);
		$this->model('MFBFriendSchedule');
		$criteria = array('com_autofbtwitter_fbfriendschedule.id'=>$id,'com_autofbtwitter_fbfriendschedule.id_user'=>intval($this->session->userdata('userid')));
		
		$schedule = $this->MFBFriendSchedule->get_by($criteria);
		if($schedule==null){
			$this->session->set_flashdata('error','Schedule can not be found');
		}else{
			if($this->MFBFriendSchedule->delete($id)==TRUE){
				$this->session->set_flashdata('info','Schedule deleted');
			}else{
				$this->session->set_flashdata('error','Schedule can not be deleted');
			}
		}
		redirect(base_url('fbfriendscheduler/index') );
	}
	
	function upload_file(){			   
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}	

	
	/*process*/
	function get_fb_users(){
		$this->model('MAccount');
		$criteria = array('id_user'=>intval($this->session->userdata('userid')),'type'=>2);
		$usernames = $this->MAccount->get_by($criteria);
		$data =array();
		if($usernames!=null){
			foreach($usernames as $user){
				$data[$user['id']] = $user['username'];
			}
		}
		return $data;
	}
	function username_check($str)
	{
		$this->model('MAccount');
		$account = $this->MAccount->get(intval($str));
		
		if($account !=null){
			if(intval($account['id_user'])==intval($this->session->userdata('userid'))){
				return TRUE;
			}
		}
		$this->form_validation->set_message('username_check','Invalid Username');
		return FALSE;
		
	}
	function checkDate($date)
	{
		if (!isset($date) || $date=="")
		{
			return FALSE;
		}
	   
		list($mm,$dd,$yy)=explode("/",$date);
		if ($dd!="" && $mm!="" && $yy!="")
		{
			return checkdate($mm,$dd,$yy);
		}
	   
		return FALSE;
	}
	function schedule_date_check($str){
		if($this->checkDate($str)==FALSE){			
			$this->form_validation->set_message('schedule_date_check','Invalid Date');
			return FALSE;
		}
		return TRUE;
		
	}
	function date_check($str){
		if($this->checkDate($str)==FALSE){			
			$this->form_validation->set_message('date_check','Invalid Date');
			return FALSE;
		}
		return TRUE;
		
	}
	function hhmm_check($str){
		list($hh,$mm)=explode(":",$str);
		if($hh!='' && $mm !=''){
			if(intval($hh)>=0 && intval($hh)<24 && intval($mm)>=0 && intval($mm)<60){
				return TRUE;
			}			
		}
		$this->form_validation->set_message('hhmm_check','Invalid Time');
		return FALSE; 
	}
	function timezone_check($str){		
		try{
			new DateTimeZone($str);
		}catch(Exception $e){
			$this->form_validation->set_message('timezone_check','Invalid Timezone');
			return FALSE;
		}
		return TRUE;

	}
	function url_check($str){
		$ret = TRUE;
		if(empty($str)){
			
		}
		else{
			//$pattern = "/^((ht|f)tp(s?)\:\/\/|~/|/)?([w]{2}([\w\-]+\.)+([\w]{2,5}))(:[\d]{1,5})?/";
			$pattern = "/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i";
			if (!preg_match($pattern, $str))
			{
				$ret = FALSE;
			}
		}
		if(!$ret)
			$this->form_validation->set_message('url_check','Invalid URL');
        return $ret;
	}
	function saveValidate()
	{		
		$this->form_validation->set_rules('fbusernames', 'lang:Sender', 'required|integer|callback_username_check');
		$this->form_validation->set_rules('username', 'lang:Username', 'required|trim|xss_clean');
		$this->form_validation->set_rules('schedule_date', 'lang:Schedule date', 'required|callback_date_check');
		$this->form_validation->set_rules('schedule_hhmm','lang:Schedule time', 'required|callback_hhmm_check');
		$this->form_validation->set_rules('schedule_tz', 'lang:Time zone', 'required|callback_timezone_check');
		$this->form_validation->set_rules('message', 'lang:Message', 'required');
		$this->form_validation->set_rules('link', 'lang:Link', 'trim|prep_url|callback_url_check|xss_clean');
		$this->form_validation->set_rules('caption', 'lang:Caption', 'trim|xss_clean');
		$this->form_validation->set_rules('image_file', 'lang:Image', 'trim|xss_clean');
		$this->form_validation->set_rules('id', 'lang:Id', 'trim|xss_clean|integer');
		return ($this->form_validation->run());
	}
	function save(){
		if($this->saveValidate()==FALSE)
		{
			$this->add();
			return;
		}
		else         
		{			
			$this->model('MFBFriendSchedule');			
			$schedule = $this->bind($this->MFBFriendSchedule);
			$schedule['com_autofbtwitter_account_id'] = intval($this->input->post('fbusernames'));			
			$schedule['id_user'] = intval($this->session->userdata('userid'));
			if($this->MFBFriendSchedule->save($schedule)==TRUE)				
				$this->session->set_flashdata('info','Schedule saved');
			else
				$this->session->set_flashdata('error','Error saving schedule');			
		}		
		redirect(base_url('fbfriendscheduler/index'));
	}	

}
?>
