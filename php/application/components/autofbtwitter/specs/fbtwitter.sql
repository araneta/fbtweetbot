-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2012 at 08:05 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fbtwitter`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `user_agent` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_account`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `username` varchar(256) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_facebook`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_facebook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_account_id` int(11) NOT NULL,
  `oauth_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_com_autofbtwitter_account` (`com_autofbtwitter_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_fbpageschedule`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_fbpageschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_fbpagetoken_id` int(11) NOT NULL,
  `schedule_date` date NOT NULL,
  `schedule_hhmm` varchar(5) NOT NULL,
  `schedule_tz` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `scheduled_date_time_server_time` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `error_message` text,
  `image_file` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_userx` (`id_user`),
  KEY `fk_com_autofbtwitter_fbpageschedule_fbpagetoken` (`com_autofbtwitter_fbpagetoken_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_fbpagetoken`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_fbpagetoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `access_token` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `page_id` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_com_autofbtwitter_fbpagetoken_user` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_fbschedule`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_fbschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_account_id` int(11) NOT NULL,
  `schedule_date` date NOT NULL,
  `schedule_hhmm` varchar(5) NOT NULL,
  `schedule_tz` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `scheduled_date_time_server_time` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `error_message` text,
  `image_file` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `caption` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_account` (`com_autofbtwitter_account_id`),
  KEY `fk_userx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_interval_type`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_interval_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `com_autofbtwitter_interval_type`
--

INSERT INTO `com_autofbtwitter_interval_type` (`id`, `name`) VALUES
(1, 'minute(s)'),
(2, 'hour(s)'),
(3, 'Day(s)'),
(4, 'Week(s)');

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_log`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` text NOT NULL,
  `created_date` datetime NOT NULL,
  `sender` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_queue`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `interval_in_minute` int(11) NOT NULL,
  `com_autofbtwitter_account_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `timezone` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `start_date_server_time` datetime NOT NULL,
  `interval` int(11) NOT NULL,
  `interval_type` int(11) NOT NULL,
  `start_hhmm` varchar(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_accountx` (`com_autofbtwitter_account_id`),
  KEY `fk_userxt` (`id_user`),
  KEY `fk_interval` (`interval_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_queue_message`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_queue_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_queue_id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `scheduled_date_time_server_time` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `error_message` text,
  PRIMARY KEY (`id`),
  KEY `fk_com_autofbtwitter_queue_message_user` (`id_user`),
  KEY `fk_com_autofbtwitter_queue_message_queue` (`com_autofbtwitter_queue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_schedule`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_account_id` int(11) NOT NULL,
  `schedule_date` date NOT NULL,
  `schedule_hhmm` varchar(5) NOT NULL,
  `schedule_tz` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_date` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `id_user` int(11) NOT NULL,
  `scheduled_date_time_server_time` datetime NOT NULL,
  `state` int(11) NOT NULL DEFAULT '0',
  `error_message` text,
  PRIMARY KEY (`id`),
  KEY `fk_account` (`com_autofbtwitter_account_id`),
  KEY `fk_userx` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `com_autofbtwitter_twitter`
--

CREATE TABLE IF NOT EXISTS `com_autofbtwitter_twitter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `com_autofbtwitter_account_id` int(11) NOT NULL,
  `oauth_token_secret` varchar(255) NOT NULL,
  `oauth_token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_com_autofbtwitter_account` (`com_autofbtwitter_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE IF NOT EXISTS `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`) VALUES
(1, 'english');

-- --------------------------------------------------------

--
-- Table structure for table `local_user`
--

CREATE TABLE IF NOT EXISTS `local_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `id_language` int(11) NOT NULL,
  `last_update` datetime NOT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_language` (`id_language`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_user`
--

CREATE TABLE IF NOT EXISTS `oauth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oauth_provider` enum('twitter','facebook') NOT NULL,
  `oauth_uid` varchar(255) NOT NULL,
  `oauth_username` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `last_update` datetime NOT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `id_language` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_oauth_user_1` (`id_language`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `fk_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `com_autofbtwitter_account`
--
ALTER TABLE `com_autofbtwitter_account`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_facebook`
--
ALTER TABLE `com_autofbtwitter_facebook`
  ADD CONSTRAINT `fk_com_autofbtwitter_account0` FOREIGN KEY (`com_autofbtwitter_account_id`) REFERENCES `com_autofbtwitter_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_fbpageschedule`
--
ALTER TABLE `com_autofbtwitter_fbpageschedule`
  ADD CONSTRAINT `fk_userx00` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_com_autofbtwitter_fbpageschedule_fbpagetoken` FOREIGN KEY (`com_autofbtwitter_fbpagetoken_id`) REFERENCES `com_autofbtwitter_fbpagetoken` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_fbpagetoken`
--
ALTER TABLE `com_autofbtwitter_fbpagetoken`
  ADD CONSTRAINT `fk_com_autofbtwitter_fbpagetoken_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_fbschedule`
--
ALTER TABLE `com_autofbtwitter_fbschedule`
  ADD CONSTRAINT `fk_account0` FOREIGN KEY (`com_autofbtwitter_account_id`) REFERENCES `com_autofbtwitter_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_userx0` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_queue`
--
ALTER TABLE `com_autofbtwitter_queue`
  ADD CONSTRAINT `fk_accountx` FOREIGN KEY (`com_autofbtwitter_account_id`) REFERENCES `com_autofbtwitter_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_userxt` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_interval` FOREIGN KEY (`interval_type`) REFERENCES `com_autofbtwitter_interval_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_queue_message`
--
ALTER TABLE `com_autofbtwitter_queue_message`
  ADD CONSTRAINT `fk_com_autofbtwitter_queue_message_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_com_autofbtwitter_queue_message_queue` FOREIGN KEY (`com_autofbtwitter_queue_id`) REFERENCES `com_autofbtwitter_queue` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_schedule`
--
ALTER TABLE `com_autofbtwitter_schedule`
  ADD CONSTRAINT `fk_account` FOREIGN KEY (`com_autofbtwitter_account_id`) REFERENCES `com_autofbtwitter_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_userx` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `com_autofbtwitter_twitter`
--
ALTER TABLE `com_autofbtwitter_twitter`
  ADD CONSTRAINT `fk_com_autofbtwitter_account` FOREIGN KEY (`com_autofbtwitter_account_id`) REFERENCES `com_autofbtwitter_account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `local_user`
--
ALTER TABLE `local_user`
  ADD CONSTRAINT `fk_language` FOREIGN KEY (`id_language`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `oauth_user`
--
ALTER TABLE `oauth_user`
  ADD CONSTRAINT `fk_oauth_user_1` FOREIGN KEY (`id_language`) REFERENCES `language` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
