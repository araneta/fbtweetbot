<?php
namespace core\Helpers;

class TimeHelper{
	/*
	 * convert string time to utc string
	 * */
	public static function convert_to_utc($localtime){
		$UTC = new \DateTimeZone("UTC");
		$serverTZ = new \DateTimeZone(date_default_timezone_get());
		$date = new \DateTime( $localtime, $serverTZ);
		$date->setTimezone( $UTC );
		return $date->format('Y-m-d H:i:s');
	}	
	public static function get_time_in_utc(){
		date_default_timezone_set('UTC');
		$utc = new \DateTimeZone("UTC");
		$date = new \DateTime();
		$date->setTimezone( $utc );
		return $date->format('Y-m-d H:i:s');
	}
	//return 2003 January 1
	public static function convert_to_string_1($utctime){		
		date_default_timezone_set('UTC');
		$newTZ = new \DateTimeZone(date_default_timezone_get());
		$date = new \DateTime($utctime, $newTZ );
		return $date->format('Y F d');
	}
	public static function convert_to_string_2($utctime){		
		date_default_timezone_set('UTC');
		$newTZ = new \DateTimeZone(date_default_timezone_get());
		$date = new \DateTime($utctime, $newTZ );
		return $date->format('g:i a');
	}
	//from Wednesday, 01 January, 2014 to 2014-1-1
	public static function convert_string_to_date_1($txt){		
		date_default_timezone_set('UTC');
		return \DateTime::createfromformat('D, d M, Y',$txt)->format('Y-m-d');
	}
	public static function convert_date_to_string_1($date){		
		date_default_timezone_set('UTC');
		return \DateTime::createfromformat('Y-m-d',$date)->format('l, d F, Y');
	}
	public static function convert_time_24_to_ampm($time){
		date_default_timezone_set('UTC');
		$d = new \DateTime($time);
		return $d->format('h:i:s A');
	}
	public static function convert_time_ampm_to_24($time){
		date_default_timezone_set('UTC');
		$d = new \DateTime($time);
		return $d->format('H:i:s');
	}
	/*
	 * compare date1 and date2
	 * return + if date2 > date1
	 * return - if date2 < date1 
	 * */
	public static function compare_date_time($date1,$date2){				
		$dateTime = new \DateTime($date1);
		$ret = $dateTime->diff( new \DateTime($date2))->format('%R');
		return $ret;		
	}
}
?>
