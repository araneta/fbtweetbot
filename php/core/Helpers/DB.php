<?php
namespace core\Helpers;

class DB {
   
    private static $pdoInstance;
	public static $host;
	public static $username;
	public static $password;
	public static $dbname;
    /*
     * Class Constructor - Create a new database connection if one doesn't exist
     * Set to private so no-one can create a new instance via ' = new DB();'
     */
    private function __construct() {}
   
    /*
     * Like the constructor, we make __clone private so nobody can clone the instance
     */
    private function __clone() {}
   
    /*
     * Returns DB instance or create initial connection
     * @param
     * @return $pdoInstance;
     */
    public static function getInstance(  ) {
           
        if(!self::$pdoInstance){			
            self::$pdoInstance = new \PDO("mysql:host=".self::$host.";dbname=".self::$dbname, self::$username, self::$password);
            self::$pdoInstance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
       
        return self::$pdoInstance;
   
    } # end method
   
    /*
     * Passes on any static calls to this class onto the singleton PDO instance
     * @param $chrMethod, $arrArguments
     * @return $mix
     */
     
    final public static function __callStatic( $chrMethod, $arrArguments ) {
           
        $pdoInstance = self::getInstance();
       
        return call_user_func_array(array($pdoInstance, $chrMethod), $arrArguments);
       
    }
	
	
} 
