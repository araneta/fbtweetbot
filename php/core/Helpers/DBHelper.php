<?php
namespace core\Helpers;

class DBHelper {
	private $db;
	function __construct(){
		$this->db  = NULL;
	}
	public function set_connection_params($host,$username,$password,$dbname){
		DB::$host = $host;
		DB::$username = $username;
		DB::$password = $password;
		DB::$dbname = $dbname;		
		$this->db  = DB::getInstance();
	}
	function get_rows($sql,$prm){		
		$statement = $this->db->prepare($sql);
		$statement->execute($prm);	
		$statement->setFetchMode(\PDO::FETCH_OBJ);
		$result = $statement->fetchAll();				
		return $result;
		
	}
	function execute($sql,$prm){
		$statement = $this->db->prepare($sql);
		return $statement->execute($prm);				
	}
	function get_scalar($sql,$prm){
		$statement = $this->db->prepare($sql);
		$statement->execute($prm);	
		$statement->setFetchMode(\PDO::FETCH_OBJ);
		$result = $statement->fetch();				
		return $result->c;
	}
	function get_last_id(){
		return $this->db->lastInsertId();
	}
	function trans_start(){
		$this->db->beginTransaction();
	}
	function trans_complete(){
		$this->db->commit();
	}
	function trans_status(){
		$this->db->inTransaction();
	}
}
