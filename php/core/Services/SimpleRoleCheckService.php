<?php
namespace core\Services;

class SimpleRoleCheckService extends BaseService{
	private $tree = array();
	private $user_roles = array(
		'public'	=> array('id'=>1,'parent'=>NULL),
		'registered'=> array('id'=>2,'parent'=>'public'),
		'member'	=> array('id'=>3,'parent'=>'registered'),
		'admin'		=> array('id'=>4,'parent'=>'registered'),
	);
	public function get_roles(){
		return $this->user_roles;
	}
	function __construct(){
		parent::__construct();
	}
	/*
	 * check whether $user_role = $required_role or 
	 * $user_role has ancestor of $required_role
	 * return boolean
	 * */	
	public function check($required_role,$user_role){
		if($required_role == $user_role)
			return TRUE;
		$roles = $this->get_roles();
		
		//check valid role	
		if(!in_array($user_role,array_keys($roles)))
			return FALSE;
		$parent = NULL;
		
		$role = $roles[$user_role];		
		$i=0;
		do{			
			$parent = $role['parent'];			
			if($parent==$required_role){
				return TRUE;
			}else{
				$role=$roles[$parent];
			}
			$i++;
			if($i==100)die('loop'.$required_role.'--'.$user_role);
		}while($role['parent']!=NULL);			
		return FALSE;
	}
}
?>
