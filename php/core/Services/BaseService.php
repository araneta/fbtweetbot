<?php
namespace core\Services;

class BaseService{	
	function __construct(){
		
	}	
	protected $errors = array();
	public function has_error(){
		if (count($this->errors)>0)
			return TRUE;
		return FALSE;
	}
	public function error_messages(){
		return implode('<br />',array_values($this->errors));	
	}
	public function error_keys(){
		return array_keys($this->errors);
	}
	public function add_error($key,$msg){
		if(array_key_exists($key,$this->errors))
			$this->errors[$key] .= $msg .'<br />';	
		else
			$this->errors[$key] = $msg;	
	}
	protected function load_template($template,$data=NULL){		
		$reflector = new \ReflectionClass(get_class($this));
		$fn = $reflector->getFileName();
		$templatefile = realpath(dirname($fn).'/../Templates'.'/'.$template.'.php');
		ob_start();		
		if($data!=NULL)
			extract($data);
		require $templatefile;
		$message = ob_get_contents();
		ob_clean();
		return $message;
	}
}
?>
