<?php
namespace core\Entities;

class PagingResult extends BaseEntity{
	public $totaldisplayrecords;
	public $totalrecords;
	public $data;
	public $start;
	public $end;
	public $page;
	public $totalpages;
	public function calculate($paging){
		$this->start = $paging->start;
		$this->end = $this->start+$this->totaldisplayrecords;
		$this->page = $paging->page;
		$this->totalpages = ceil($this->totalrecords/$paging->pagesize);
	}
}
