<?php
namespace core\Entities;

class Ical extends BaseEntity{
	public $start;
	public $endtime;
	public $title;
	public $uid;
	public $location;
	
	public function __construct(){
		parent::__construct();
		$this->location='';
	}
}
