<?php
namespace core;

abstract class BaseFactory
{
	
	/**
	 * Global database object
	 *
	 * @var    DBHelper
	 * @since  1
	 */
	public static $database = null;
	/**
	 * Get a database object.
	 *
	 * Returns the global {@link DBHelper} object, only creating it if it doesn't already exist.
	 *
	 * @return  DBHelper
	 *
	 * @see     DBHelper
	 * @since   1
	 */
	public static function get_database()
	{
		if (!self::$database)
		{
			self::$database = self::create_database_object();
		}

		return self::$database;
	}
	public static function set_database($dbinstance){
		self::$database = $dbinstance;
	}
	abstract public function create_database_object();
}
